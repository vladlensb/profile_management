from __future__ import unicode_literals
from requests_oauthlib import OAuth1
from urlparse import parse_qs
#import controller for inheritance
from openerp.addons.auth_oauth.controllers.main import OAuthLogin as Login
from openerp.addons.auth_oauth.controllers.main import OAuthController as OAController
#import linkedin Supported file.
from linkedin import linkedin
from enum import Enum
from linkedin import server
# -*- encoding: utf-8 -*-

import requests

import functools
import logging

import simplejson
import urlparse
import werkzeug.utils
import oauth2 as oauth
import urllib, urllib2
from werkzeug.exceptions import BadRequest

import openerp
from openerp import SUPERUSER_ID
from openerp import http
from openerp.http import request
from openerp.addons.web.controllers.main import db_monodb, ensure_db, set_cookie_and_redirect, login_and_redirect
#from openerp.addons.auth_signup.controllers.main import AuthSignupHome as Home
from openerp.modules.registry import RegistryManager
from openerp.tools.translate import _

_logger = logging.getLogger(__name__)

REQUEST_TOKEN_URL = "https://api.twitter.com/oauth/request_token"
AUTHORIZE_URL = "https://api.twitter.com/oauth/authorize?oauth_token="
ACCESS_TOKEN_URL = "https://api.twitter.com/oauth/access_token"


#Code for Twitter : THERMEVO local   LOCAL SEREVER
#CONSUMER_KEY = "GQ2yqrKJixXlnAK0OUlqH0gfx"
#CONSUMER_SECRET = "dZD19pwuYtpYigZsMvmboSOBwCmWGuGC5OiZMgwBBktzarOHt9"

#Code for Twitter : THERMEVO test  TEST SERVER
#CONSUMER_KEY = "qJOAkpanMLNw6PPi1Y5z0abpp"
#CONSUMER_SECRET = "XXfs4prcblFUSHqtNtlfNQTdPjw0HjkXfhOMI0G53DtJMhHpOz"

#Code for Twitter :THERMEVO  MAIN SERVER
CONSUMER_KEY = "jjZWe5Y0PmMXsRKkIwR9gOzpg"
CONSUMER_SECRET = "T9t3cmbLnKFOjnDmG6gMJj0TaPvuuKBmZrM9YIL3j8Z7kDfJ5n"

OAUTH_TOKEN = ""
OAUTH_TOKEN_SECRET = ""
#----------------------------------------------------------
# helpers
#----------------------------------------------------------
def fragment_to_query_string(func):
    @functools.wraps(func)
    def wrapper(self, *a, **kw):
        if not kw:
            return """<html><head><script>
                var l = window.location;
                var q = l.hash.substring(1);
                var r = l.pathname + l.search;
                if(q.length !== 0) {
                    var s = l.search ? (l.search === '?' ? '' : '&') : '?';
                    r = l.pathname + l.search + s + q;
                }
                if (r == l.pathname) {
                    r = '/';
                }
                window.location = r;
            </script></head><body></body></html>"""
        return func(self, *a, **kw)
    return wrapper


def setup_oauth():

    """Authorize your app via identifier."""
    # Request token
    oauth = OAuth1(CONSUMER_KEY, client_secret=CONSUMER_SECRET)
    r = requests.post(url=REQUEST_TOKEN_URL, auth=oauth)
    credentials = parse_qs(r.content)

    resource_owner_key = credentials.get('oauth_token')[0]
    resource_owner_secret = credentials.get('oauth_token_secret')[0]

    # Authorize
    authorize_url = AUTHORIZE_URL + resource_owner_key
    print "Resource Owner Key ====> ",resource_owner_key
    print 'Please go here and authorize: ' + authorize_url
    return resource_owner_key
#----------------------------------------------------------
# Controller
#----------------------------------------------------------

class OAuthLogin_inherit(Login):
    def list_providers(self):
        try:
            provider_obj = request.registry.get('auth.oauth.provider')
            providers = provider_obj.search_read(request.cr, SUPERUSER_ID, [('enabled', '=', True), ('auth_endpoint', '!=', False), ('validation_endpoint', '!=', False)])
            # TODO in forwardport: remove conditions on 'auth_endpoint' and 'validation_endpoint' when these fields will be 'required' in model
        except Exception:
            providers = []
        for provider in providers:
            print "========================provider['auth_endpoint']===============",provider['auth_endpoint']
            return_url = request.httprequest.url_root + 'auth_oauth/signin'
            state = self.get_state(provider)
            params = dict(
                debug=request.debug,
                response_type='token',
                client_id=provider['client_id'],
                redirect_uri=return_url,
                scope=provider['scope'],
                state=simplejson.dumps(state),
            )
            if provider['name'].upper() == "LINKEDIN":
               params = dict(
                debug=request.debug,
                response_type='code',
                client_id=provider['client_id'],
                redirect_uri=return_url,
                scope=provider['scope'],
                state=simplejson.dumps(state),
                )
            elif provider['name'].upper() == "TWITTER":
                oauth_token_val = setup_oauth()
                print "------Twitter URL is Ready----------->>>>>> ",provider['auth_endpoint']
                params = dict(
                oauth_token=oauth_token_val,
                )

            provider['auth_link'] = provider['auth_endpoint'] + '?' + werkzeug.url_encode(params)
        return providers
class OAuthController_inherit(OAController):

    @http.route('/auth_oauth/signin', type='http', auth='none')
    @fragment_to_query_string
    def signin(self, **kw):
        cr, uid, context, registry = request.cr, request.uid, request.context, request.registry
        site_url = request.httprequest.url_root
        state = ""
        print "=====Value of KW is ====>>>>===KWWW", kw
        if "oauth_token" in kw.keys() and len(kw)==2:
            url = "u'"+ site_url +"web'"
            consumer=oauth.Consumer(CONSUMER_KEY,CONSUMER_SECRET)
            client=oauth.Client(consumer)
            resp, content = client.request(REQUEST_TOKEN_URL, "GET")
            if resp['status'] != '200':
                raise Exception("Invalid response %s." % resp['status'])
            request_token = dict(urlparse.parse_qsl(content))
            kw[str("request_token")]=request_token
            kw[str("consumer")]=consumer
            kw[str('access_token')] = kw['oauth_token']
            #Code for local machine
            #state =  {'p': 4, 'r': url, 'd': cr.dbname}
            #Code for THERMEVO machine
            state =  {'p': 7, 'r': url, 'd': cr.dbname}

            #Code for test THERMEVO machine
            #state =  {'p': 4, 'r': url, 'd': cr.dbname}
            print "\n\n\nCreated State is ======> ",state


        if "state" in kw.keys():
            state = simplejson.loads(kw['state'])
        #Linked IN API return first code so using code we get access token...
        if "code" in kw.keys():
            if (kw['code'] != ""):
                #Code for LinkedIn Authentication for local server.
                #API_KEY = "7535v0js0230c7"
                #API_SECRET = "c598s1Mr5EV9T5dp"

                #Code for LinkedIn Authentication for test.THERMEVO.de server.
                # API_KEY = "75hvbgb3652sua"
                # API_SECRET = "O15LJgRR60mZr1uu"

                #Code for LinkedIn Authentication for THERMEVO.de server.
                API_KEY = "75g563bwep5x79"
                API_SECRET = "PMBjieM879epCx4N"
                
                RETURN_URL = site_url + "auth_oauth/signin"
                authentication = linkedin.LinkedInAuthentication(API_KEY, API_SECRET, RETURN_URL, linkedin.PERMISSIONS.enums.values())
                application = linkedin.LinkedInApplication(authentication)
                authentication.authorization_code = kw['code']
                token = authentication.get_access_token()
                kw['access_token'] = token.access_token
                #print "\n\n\nAccess Token is =======>",token.access_token
        print "Actual value of State is ===> ",state
        dbname = state['d']
        provider = state['p']
        context = state.get('c', {})
        context = {}
        registry = RegistryManager.get(dbname)
        with registry.cursor() as cr:
            try:
                u = registry.get('res.users')
                credentials = u.auth_oauth(cr, SUPERUSER_ID, provider, kw, context=None)
                cr.commit()
                action = state.get('a')
                menu = state.get('m')
                redirect = werkzeug.url_unquote_plus(state['r']) if state.get('r') else False
                url = '/page/personal_profile?'
                if redirect:
                    url = redirect
                elif action:
                    url = '/web#action=%s' % action
                elif menu:
                    url = '/web#menu_id=%s' % menu
                url = url.replace('/web','/page/personal_profile')
                return login_and_redirect(*credentials, redirect_url=url)
            except AttributeError:
                # auth_signup is not installed
                print "\n\n\n Error while login.........."
                _logger.error("auth_signup not installed on database %s: oauth sign up cancelled." % (dbname,))
                url = "/page/login?oauth_error=1"
            except openerp.exceptions.AccessDenied:
                # oauth credentials not valid, user could be on a temporary session
                _logger.info('OAuth2: access denied, redirect to main page in case a valid session exists, without setting cookies')
                url = "/page/login?oauth_error=3"
                redirect = werkzeug.utils.redirect(url, 303)
                redirect.autocorrect_location_header = False
                return redirect
            except Exception, e:
                # signup error
                _logger.exception("OAuth2: %s" % str(e))
                url = "/page/login?oauth_error=2"

        return set_cookie_and_redirect(url)    
# vim:expandtab:tabstop=4:softtabstop=4:shiftwidth=4:
    