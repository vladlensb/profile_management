# -*- coding: UTF-8 -*-

##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2016- Libre Comunication (<>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import osv, fields
from openerp.tools.translate import _
from openerp import SUPERUSER_ID
import logging

_logger = logging.getLogger(__name__)


class CrmLead(osv.osv):
    """ CRM Lead Case """
    _inherit = "crm.lead"

    _columns = {
        'ch': fields.boolean(srting='ch',
                             default=True),
        'ch_team': fields.boolean(srting='ch_team',
                                  default=True),
        'sale_order_ids': fields.one2many('sale.order',
                                          'crm_lead_id',
                                          string='Sale Orders'),
        'part_pallet': fields.boolean('Part pallet'),
        'pallet_product': fields.many2one('product.product',
                                          string="Pallet product")
    }

    def write(self, cr, uid, ids, values, context=None):
        return super(CrmLead, self).write(
            cr, uid, ids, values, context=context)

    def read_group(self, cr, uid, domain, fields, groupby, offset=0, limit=None,
                   context=None, orderby=False, lazy=True):

        crm_case_section_pool = self.pool.get('crm.case.section')
        crm_case_section_ids = crm_case_section_pool.search(
            cr, uid, [], context=context)
        crm_case_sections = crm_case_section_pool.browse(
            cr, uid, crm_case_section_ids, context=context)

        res_users_pool = self.pool.get('res.users')
        res_user = res_users_pool.browse(cr, uid, uid, context=context)

        section_ids = []
        for crm_case_section in crm_case_sections:
            if res_user in crm_case_section.member_ids:
                section_ids.append(crm_case_section.id)
        result = []
        if section_ids:
            cr.execute("""
                        SELECT id
                        FROM crm_lead
                        WHERE section_id IN %s
                        """, (tuple(section_ids),))
            result = [x[0] for x in cr.fetchall()]
        lead_ids = []
        false_lead_ids = []
        for lead_id in self.search(cr, uid, [], context=context):
            if lead_id in result:
                lead_ids.append(lead_id)
            else:
                false_lead_ids.append(lead_id)
        self.write(cr, SUPERUSER_ID, lead_ids,
                   {'ch_team': True}, context=context)
        self.write(cr, SUPERUSER_ID, false_lead_ids,
                   {'ch_team': False}, context=context)

        return super(CrmLead, self).read_group(cr, uid, domain, fields,
                                               groupby, offset, limit, context,
                                               orderby, lazy)
