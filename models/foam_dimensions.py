# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Business Applications
#    Copyright (c) 2012 OpenERP S.A. <http://openerp.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from openerp.osv import osv, fields
from docutils.nodes import Invisible


class FoamDimensions(osv.osv):
    _name = "foam.dimensions"
    _columns = {
        'width': fields.float('Width'),
        'height': fields.float('height'),
        'name': fields.text('Name', translate=True),
        'order_line_id': fields.many2one('sale.order.line', 'Line Id'),
        'foam_qty': fields.integer('Quantity'),
        'price': fields.float('Foam Price')
    }

    def create(self, cr, uid, vals, context=None):
        vals['name'] = str(vals['width']) + " * " + str(vals['height'])
        new_id = super(FoamDimensions, self).create(
            cr, uid, vals, context=context)
        return new_id
