# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Business Applications
#    Copyright (c) 2012 OpenERP S.A. <http://openerp.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from openerp.osv import osv, fields
from docutils.nodes import Invisible


class ProductAttributeValue(osv.osv):
    _inherit = "product.attribute.value"
    _sql_constraints = [('value_company_uniq',
                         'CHECK(1=1)',
                         'This attribute value already exists !')]

    def _get_user_fnc(self, cr, uid, ids, field_name, arg, context):
        res = {}
        for id in ids:
            current_user = self.pool.get('res.users').browse(cr, uid, uid)
            res[id] = current_user

        return res

    _columns = {
        'product_custom_length_ids': fields.one2many(
            'product.custom.length',
            'product_attribute_line_id',
            'Lengths'),
        'user_id': fields.function(_get_user_fnc,
                                   type="integer",
                                   method=True,
                                   string='User Id'),
        'res_id': fields.integer('resource id'),
    }


class ProductCustomlength(osv.osv):
    _name = 'product.custom.length'
    _columns = {
        'user_id': fields.integer('user_id'),
        'quantity': fields.float('quantity'),
        'product_attribute_line_id': fields.many2one(
            'product.attribute.value',
            'Product Attribute Line Id'),
        'product_id': fields.many2one('product.template',
                                      'Product Data'),
    }
