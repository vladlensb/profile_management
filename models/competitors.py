# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Business Applications
#    Copyright (c) 2012 OpenERP S.A. <http://openerp.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from openerp.osv import osv, fields
from docutils.nodes import Invisible


class Competitors(osv.osv):
    _name = "competitors"
    _columns = {
        'name': fields.char(string='Name',
                            translate=True,
                            size=25),
        'products': fields.one2many('competitors.products',
                                    'id',
                                    string='Products'),
    }


class CompetitorsProducts(osv.osv):
    _name = "competitors.products"
    _columns = {
        'name': fields.char(string='Product Name',
                            translate=True,
                            size=25),
        'area': fields.float(string='Area(mm²)'),
        'article': fields.char(string='Article Number',
                               translate=True,
                               size=25),
        'competitor_id': fields.many2one('competitors',
                                         string='Competitor'),
        'competitor_product_image1': fields.binary(
            string="Image",
            help="This field holds the image used as image for the competitors product."),
        'product_ids': fields.many2many('product.template',
                                        'competitors_product_template_rel',
                                        'competitor_product_id',
                                        'product_id',
                                        string='Products'),
    }


class InheritProductAttribute(osv.osv):
    _inherit = 'product.attribute'
    _columns = {
        'type': fields.selection([('radio', 'Radio'),
                                  ('select', 'Select'),
                                  ('color', 'Color'),
                                  ('hidden', 'Hidden'),
                                  ('text', 'Text')],
                                 string="Type",
                                 type="char"),
    }
    _defaults = {
        'type': lambda *a: 'radio',
    }

    def adding(self, cr, uid, ids, user_text, context=None):
        print "User Text is ====>", user_text


class CompetitorsLine(osv.osv):
    _name = "competitors.line"
    _columns = {
        'product_id': fields.many2one('product.template',
                                      string='Product'),
        'competitors_id': fields.many2one('competitors',
                                          string="Competitors"),
        'competitors_product': fields.many2one('competitors.products',
                                               string="Competitors Product"),
    }
