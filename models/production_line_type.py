# -*- coding: UTF-8 -*-

##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2016-Today Libre Comunication (<erpsystem.com.ua@gmail.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp import fields, models, api, _
from openerp.exceptions import ValidationError


class ProductionLineType(models.Model):
    _name = "production.line.type"

    name = fields.Char(string="Name")
    maintenance_order = fields.Char(string="Maintenance Order")


class ProductionSchemeConfig(models.Model):
    _name = "production.scheme.config"

    spreadsheet_id = fields.Char(string='SpreadsheetId')
    range_names = fields.Char(string='Range Names')

    @api.model
    def create(self, vals):
        if self.search([]):
            raise ValidationError(_("You can not create more config element"))
        return super(ProductionSchemeConfig, self).create(vals)
