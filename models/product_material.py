# -*- coding: UTF-8 -*-

##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2016-Today Libre Comunication (<erpsystem.com.ua@gmail.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


from openerp.osv import osv, fields


class ProductMaterial(osv.osv):
    _name = "product.material"
    _description = "Product Material"
    _order = "sequence"

    _columns = {
        'name': fields.char('Material Name', translate=True),
        'material_article_number': fields.char('Material Article Number'),
        'material_color': fields.char('Material Color', default='0'),
        'activ': fields.boolean('Active'),
        'sequence': fields.integer('Sequence'),
        'density': fields.float('Density'),
        'additional_pur': fields.boolean('Additional Pur'),
        'additional_pe': fields.boolean('Additional Pe'),
        'additional_gluewire': fields.boolean('Additional Gluewire'),
        'gluewire_cost': fields.float('Gluewire Cost, EUR'),
        'additional_foil': fields.boolean('Additional Foil'),
        'additional_paint_protection': fields.boolean(
            'Additional Paint Protection'),

        'processing_type': fields.selection([
            ('extruziya', 'Extruziya'),
            ('polyuretan', 'Polyuretan')], 'Processing Type'),
        'restriction_material_length_min': fields.float(
            'Restriction Material Length Min'),
        'restriction_material_length_max': fields.float(
            'Restriction Material Length Max'),
        'restriction_material_length_step': fields.float(
            'Restriction Material Length Step'),
        'raw_material_price': fields.float('Raw Material Price'),
        'material_waist': fields.float('Material Waist'),
        'material_width_min': fields.float('Restriction Material Width Min'),
        'restriction_material_width_step': fields.float(
            'Restriction Material Width Step'),
        'restriction_material_height_min': fields.float(
            'Restriction Material Height Min'),
        'restriction_material_height_max': fields.float(
            'Restriction Material Height Max'),
        'restriction_material_height_step': fields.float(
            'Restriction Material Height Step'),

        'categ_id': fields.many2one('product.category',
                                    'Customer Internal Category',
                                    required=True),
        'thermevo_categ_id': fields.many2one('product.category',
                                             'Thermevo Internal Category',
                                             required=True),
        'foam': fields.boolean('Foam'),

        'minimum_purchase': fields.integer('Minimum Purchase'),
        'table_name_material': fields.char('Table Name Material',
                                           translate=True),

        'filter_name': fields.char('Filter Name', translate=True),
        'product_extruder_group_id': fields.many2one(
            'product.extruder.group',
            string='Product Extruder Group'),

        'further_processing_ids': fields.many2many(
            "further.processing",
            'further_processing_material_rel',
            'further_processing_id',
            'material_id',
            string='Further Processing'),

    }

    _default = {
        'minimum_purchase': 5000,
    }

    def onchange_additional_pur(self, cr, uid, ids, additional_pur,
                                context=None):
        if additional_pur:
            return {'value': {'additional_pe': False}}
        else:
            return {}

    def onchange_additional_pe(self, cr, uid, ids, additional_pe, context=None):
        if additional_pe:
            return {'value': {'additional_pur': False}}
        else:
            return {}

    def create(self, cr, uid, values, context=None):
        filter_name = (
            values.get('name').lower().replace(' ', '_').replace(
                '%', '').replace('/', ''))
        values['filter_name'] = filter_name
        return super(ProductMaterial, self).create(cr, uid, values,
                                                   context=context)

    def write(self, cr, uid, ids, values, context=None):
        if values.get('name'):
            filter_name = (
                values.get('name').lower().replace(
                    ' ', '_').replace('%', '').replace('/', ''))
            values['filter_name'] = filter_name
        return super(ProductMaterial, self).write(cr, uid, ids, values,
                                                  context=context)


class ProductExtruderGroup(osv.osv):
    _name = 'product.extruder.group'

    _columns = {
        'name': fields.char('Name')
    }
