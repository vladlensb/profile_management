# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Business Applications
#    Copyright (c) 2012 OpenERP S.A. <http://openerp.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
import openerp.addons.decimal_precision as dp
from openerp.osv import osv, fields
from openerp.tools.translate import _
from openerp.addons.web.http import request
from openerp import SUPERUSER_ID
import logging
from openerp import models, api

_logger = logging.getLogger(__name__)


def change_default_code(obj, product, order_line):
    new_default_code = product.default_code
    if order_line:
        try:
            order_line = obj.pool.get('sale.order.line').browse(
                request.cr, request.uid, order_line.id, context=request.context)
        except:
            order_line = obj.get('sale.order.line').browse(
                request.cr, request.uid, order_line.id, context=request.context)
        old_default_code = product.default_code

        default_code_dict = old_default_code.split('/')
        if len(default_code_dict) > 1:
            length = default_code_dict[1]
        else:
            length = product.length
        static_default_code = default_code_dict[0].split('.')[0]

        geometry = 2

        if order_line.gluewire:
            if order_line.color_laser_marking == 'no':
                geometry = 2
            if order_line.color_laser_marking != 'no':
                if not order_line.print_logo:
                    geometry = 4
                else:
                    if order_line.color_laser_marking == 'grey':
                        geometry = 6
                    else:
                        geometry = 8
        else:
            if order_line.color_laser_marking != 'no':
                if not order_line.print_logo:
                    geometry = 3
                else:
                    if order_line.color_laser_marking == 'grey':
                        geometry = 5
                    else:
                        geometry = 7
            else:
                if order_line.e_low_open_groove:
                    geometry = 0
                else:
                    geometry = 1
        further = 0
        laser = 0
        foam = 0
        color = 0

        default_code = u'%s%s%s%s%s'

        # for further_processing in order_line.further_processing_ids:
        #     further = further_processing.processing_article_number

        if order_line.product_id.material_id.material_color:
            color = order_line.product_id.material_id.material_color

        new_default_code = u'%s.%s/%s' % (
            static_default_code,
            default_code % (geometry, further, laser, foam, color),
            length)
    return new_default_code


class SaleOrder(osv.osv):
    _inherit = 'sale.order'

    def _get_check_team(self, cr, uid, ids, name, arg, context=None):
        res = {}
        crm_case_section_pool = self.pool.get('crm.case.section')
        crm_case_section_ids = crm_case_section_pool.search(cr, uid, [],
                                                            context=context)
        crm_case_sections = crm_case_section_pool.browse(
            cr, uid, crm_case_section_ids, context=context)

        res_users_pool = self.pool.get('res.users')
        res_user = res_users_pool.browse(cr, uid, uid, context=context)

        section_ids = []
        for crm_case_section in crm_case_sections:
            if res_user in crm_case_section.member_ids:
                section_ids.append(crm_case_section.id)
        result = []
        if section_ids:
            cr.execute("""
                SELECT id
                FROM sale_order AS so
                WHERE so.section_id IN %s
                """, (tuple(section_ids),))

            result = [x[0] for x in cr.fetchall()]
        order_ids = []
        false_order_ids = []
        for order_id in self.search(cr, uid, [], context=context):
            res[order_id] = False
            if order_id in result:
                res[order_id] = True,
                order_ids.append(order_id)
            else:
                false_order_ids.append(order_id)
        self.write(
            cr, SUPERUSER_ID, order_ids, {'ch_team': True}, context=context)
        self.write(cr, SUPERUSER_ID, false_order_ids, {'ch_team': False},
                   context=context)
        return res

    def action_invoice_create(self, cr, uid, ids, grouped=False, states=None,
                              date_invoice=False, context=None):
        for order in self.browse(cr, uid, ids, context=context):
            for line in order.order_line:
                line.write({'state': 'manual'})
        states = ['confirmed', 'sent', 'done', 'exception', 'manual']
        return super(SaleOrder, self).action_invoice_create(
            cr, uid, ids, grouped=grouped, states=states,
            date_invoice=date_invoice, context=context)

    def _product_margin(self, cr, uid, ids, field_name, arg, context=None):
        result = {}
        for sale in self.browse(cr, uid, ids, context=context):
            result[sale.id] = 0.0
            for line in sale.order_line:
                if line.state == 'cancel':
                    continue
                result[sale.id] += line.margin or 0.0
        return result

    def _get_order(self, cr, uid, ids, context=None):
        result = {}
        for line in self.pool.get('sale.order.line').browse(cr, uid, ids,
                                                            context=context):
            result[line.order_id.id] = True
        return result.keys()

    def _get_delivery_count(self, cr, uid, ids, field_name, arg, context=None):
        result = {}
        sale_orders = self.browse(cr, uid, ids, context=context)

        procurement_order_pool = self.pool.get('procurement.order')
        stock_move_pool = self.pool.get('stock.move')

        for order in sale_orders:
            header = u"<div class='col-md-2'></div>" \
                     u"<div class='col-md-8'>" \
                     u'<table width="800">' \
                     u'    <thead>' \
                     u'        <tr style="border-bottom:1px solid grey">' \
                     u'            <td width="600" ' \
                     u'                style="padding-left: 10px;text-align: center;">' \
                     u'                <strong>Product</strong>' \
                     u'            </td>' \
                     u'            <td width="100" ' \
                     u'                style="padding-left: 10px;text-align: center;">' \
                     u'                <strong>Delivery Quantity</strong>' \
                     u'            </td>' \
                     u'            <td width="100" ' \
                     u'                style="padding-left: 10px;text-align: center;">' \
                     u'                <strong>Left to Deliver</strong>' \
                     u'            </td>' \
                     u'        </tr>' \
                     u'    </thead>' \
                     u'    <tbody>' \
                     u'    {body}' \
                     u'    </tbody>' \
                     u"</div>" \
                     u"<div class='col-md-2'/>"

            body = ''

            for line in order.order_line:
                procurement = procurement_order_pool.search(
                    cr, uid, [('sale_line_id', '=', line.id)])

                if procurement:
                    move_line_ids = stock_move_pool.search(
                        cr, uid, [('procurement_id', 'in', procurement),
                                  ('origin_returned_move_id', '=', False),
                                  ('state', '=', 'done')])
                    move_lines = stock_move_pool.browse(
                        cr, uid, move_line_ids, context=context)
                    amount = 0
                    for move in move_lines:
                        amount += move.product_qty

                    body += u'<tr style="border-bottom:1px solid grey">' \
                            u'  <td style="padding-left: 10px;">' \
                            u'      {product}' \
                            u'  </td>' \
                            u'  <td align="center" ' \
                            u'      style="padding-left:10px;text-align: center;">' \
                            u'      {qty1}' \
                            u'  </td>' \
                            u'  <td align="center" ' \
                            u'      style="padding-left:10px;text-align: center;">' \
                            u'      {qty2}' \
                            u'  </td>' \
                            u'</tr>'.format(product=line.name,
                                            qty1=amount,
                                            qty2=line.product_uom_qty - amount)
            table = header.format(body=body)

            result[order.id] = table
        return result

    _columns = {

        'name': fields.char('Order Reference',
                            required=True,
                            copy=False,
                            readonly=True,
                            states={'draft': [('readonly', False)],
                                    'sent': [('readonly', False)],
                                    'user_confirmed': [('readonly', False)],
                                    'manual': [('readonly', False)]},
                            select=True),
        'date_order': fields.datetime(
            'Date',
            required=True,
            readonly=True,
            select=True,
            states={'draft': [('readonly', False)],
                    'sent': [('readonly', False)],
                    'user_confirmed': [('readonly', False)],
                    'manual': [('readonly', False)]},
            copy=False),
        'user_id': fields.many2one(
            'res.users',
            'Salesperson',
            states={'draft': [('readonly', False)],
                    'sent': [('readonly', False)],
                    'user_confirmed': [('readonly', False)],
                    'manual': [('readonly', False)]},
            select=True,
            track_visibility='onchange'),
        'partner_id': fields.many2one(
            'res.partner',
            'Customer',
            readonly=True,
            states={'draft': [('readonly', False)],
                    'sent': [('readonly', False)],
                    'user_confirmed': [('readonly', False)],
                    'manual': [('readonly', False)]},
            required=True,
            change_default=True,
            select=True,
            track_visibility='always'),
        'partner_invoice_id': fields.many2one(
            'res.partner',
            'Invoice Address',
            readonly=True,
            required=True,
            states={'draft': [('readonly', False)],
                    'sent': [('readonly', False)],
                    'user_confirmed': [('readonly', False)],
                    'progress': [('readonly', False)],
                    'manual': [('readonly', False)]},
            help="Invoice address for current sales order."),
        'partner_shipping_id': fields.many2one(
            'res.partner',
            'Delivery Address',
            readonly=True,
            required=True,
            states={'draft': [('readonly', False)],
                    'sent': [('readonly', False)],
                    'user_confirmed': [('readonly', False)],
                    'progress': [('readonly', False)],
                    'manual': [('readonly', False)]},
            help="Delivery address for current sales order."),
        'pricelist_id': fields.many2one(
            'product.pricelist',
            'Pricelist',
            required=True,
            readonly=True,
            states={'draft': [('readonly', False)],
                    'sent': [('readonly', False)],
                    'user_confirmed': [('readonly', False)],
                    'manual': [('readonly', False)]},
            help="Pricelist for current sales order."),
        'project_id': fields.many2one(
            'account.analytic.account',
            'Contract / Analytic',
            readonly=True,
            states={'draft': [('readonly', False)],
                    'sent': [('readonly', False)],
                    'user_confirmed': [('readonly', False)],
                    'manual': [('readonly', False)]},
            help="The analytic account related to a sales order."),

        'order_line': fields.one2many(
            'sale.order.line',
            'order_id',
            'Order Lines',
            readonly=False,
            states={'draft': [('readonly', False)],
                    'sent': [('readonly', False)],
                    'user_confirmed': [('readonly', False)],
                    'progress': [('readonly', False)],
                    'manual': [('readonly', False)]},
            copy=True),

        'picking_policy': fields.selection(
            [('direct', 'Deliver each product when available'),
             ('one', 'Deliver all products at once')],
            'Shipping Policy',
            required=True,
            readonly=True,
            states={'draft': [('readonly', False)],
                    'sent': [('readonly', False)],
                    'user_confirmed': [('readonly', False)],
                    'manual': [('readonly', False)]},
            help="""Pick 'Deliver each product when available'
            if you allow partial delivery."""),
        'order_policy': fields.selection([
            ('manual', 'On Demand'),
            ('picking', 'On Delivery Order'),
            ('prepaid', 'Before Delivery'),
        ], 'Create Invoice',
            required=True,
            readonly=True,
            states={'draft': [('readonly', False)],
                    'sent': [('readonly', False)],
                    'user_confirmed': [('readonly', False)],
                    'manual': [('readonly', False)]},
            help="""On demand: A draft invoice can be created from the sales
            order when needed. \nOn delivery order: A draft invoice can be
             created from the delivery order when the products have been
             delivered. \nBefore delivery: A draft invoice is created from
              the sales order and must be paid before the products
              can be delivered."""),

        'template_id': fields.many2one(
            'sale.quote.template',
            'Quote Template',
            readonly=True,
            states={'draft': [('readonly', False)],
                    'sent': [('readonly', False)],
                    'user_confirmed': [('readonly', False)],
                    'manual': [('readonly', False)]}, ),

        'order_category_ids': fields.one2many(
            'sale.order.category',
            'order_id',
            'Order categories',
            states={'draft': [('readonly', False)],
                    'sent': [('readonly', False)],
                    'user_confirmed': [('readonly', False)],
                    'manual': [('readonly', False)]}, ),
        'location': fields.char(
            'Location',
            states={'draft': [('readonly', False)],
                    'sent': [('readonly', False)],
                    'user_confirmed': [('readonly', False)],
                    'manual': [('readonly', False)]},
        ),
        'state': fields.selection(
            [('cancel', 'Cancelled'),
             ('draft', 'Draft'),
             ('sent', 'Quotation Sent'),
             ('user_confirmed', 'User Confirmed'),
             ('manual', 'Sale to Invoice'),
             ('progress', 'Sales Order'),
             ('confirmed', 'Confirmed'),
             ('exception', 'Exception'),
             ('waiting_date', 'Waiting Schedule'),
             ('shipping_except', 'Shipping Exception'),
             ('invoice_except', 'Invoice Exception'),
             ('done', 'Done'),
             ],
            'Status', required=True, readonly=True, copy=False,
            help='* The \'Draft\' status is set when the related sales order in draft status. \
                    \n* The \'Confirmed\' status is set when the related sales order is confirmed. \
                    \n* The \'Exception\' status is set when the related sales order is set as exception. \
                    \n* The \'Done\' status is set when the sales order line has been picked. \
                    \n* The \'Cancelled\' status is set when a user cancel the sales order related.'),
        'ch': fields.function(_get_check_team, type='boolean', srting='ch'),
        'ch_team': fields.boolean(srting='ch_team'),
        'crm_lead_id': fields.many2one('crm.lead',
                                       string='Opportunity'),
        'default_pricelist': fields.boolean(
            srting='Default Pricelist for Opportunity',
            copy=False),
        'margin': fields.function(_product_margin, string='Margin',
                                  help="It gives profitability by calculating the difference between the Unit Price and the cost price.",
                                  store={
                                      'sale.order.line':
                                          (_get_order,
                                           ['margin', 'purchase_price',
                                            'order_id', 'price_unit'], 20),
                                      'sale.order': (
                                          lambda self, cr, uid, ids, c={}: ids,
                                          ['order_line'], 20),
                                  }, digits_compute=dp.get_precision(
                'Product Price')),
        'delivery_lines_count': fields.function(_get_delivery_count,
                                                type='html',
                                                string="delivery_infotmation")
    }

    def onchange_default_pricelist(
            self, cr, uid, ids, default_pricelist, crm_lead_id, context=None):
        if default_pricelist and crm_lead_id:
            try:
                s_id = ids[0]
            except:
                s_id = ids
            sale_order_id = self.search(
                cr, uid, [('crm_lead_id', '=', crm_lead_id),
                          ('id', '!=', s_id),
                          ('default_pricelist', '=', True)])
            if sale_order_id:
                sale_order = self.browse(
                    cr, uid, sale_order_id, context=context)
                sale_order_name = ', '.join(
                    so.name for so in sale_order)
                return {
                    'warning': {
                        'title': _('Error'),
                        'message': _(
                            'You have default pricelist for this contract: %s')
                                   % sale_order_name
                    },
                    'value': {'default_pricelist': False}
                }

        return True

    def write(self, cr, uid, ids, values, context=None):
        if values.get('default_pricelist'):
            sale_orders = self.browse(cr, uid, ids, context=context)
            for sale in sale_orders:
                sale_order_id = None

                if values.get('crm_lead_id'):
                    sale_order_id = self.search(
                        cr, uid,
                        [('crm_lead_id', '=', values.get('crm_lead_id')),
                         ('id', '!=', sale.id),
                         ('default_pricelist', '=', True)])
                elif sale.crm_lead_id:
                    sale_order_id = self.search(
                        cr, uid, [('crm_lead_id', '=', sale.crm_lead_id.id),
                                  ('id', '!=', sale.id),
                                  ('default_pricelist', '=', True)])

                if sale_order_id:
                    sale_order = self.browse(
                        cr, uid, sale_order_id, context=context)
                    sale_order_name = ', '.join(
                        so.name for so in sale_order)
                    raise osv.except_osv(
                        _("Error"),
                        _("You have default pricelist for this contract: %s"
                          % sale_order_name))

        return super(SaleOrder, self).write(
            cr, uid, ids, values, context=context)

    def onchange_partner_id(self, cr, uid, ids, part, context=None):
        if not part:
            return {'value': {'partner_invoice_id': False,
                              'partner_shipping_id': False,
                              'payment_term': False,
                              'fiscal_position': False}}
        part = self.pool.get('res.partner').browse(
            cr, uid, part, context=context)
        addr = self.pool.get('res.partner').address_get(
            cr, uid, [part.id], ['delivery', 'invoice', 'contact'])
        pricelist = part.property_product_pricelist \
                    and part.property_product_pricelist.id or False

        invoice_part = self.pool.get('res.partner').browse(
            cr, uid, addr['invoice'], context=context)
        payment_term = invoice_part.property_payment_term \
                       and invoice_part.property_payment_term.id or False
        # dedicated_salesman = part.user_id and part.user_id.id or uid
        val = {
            'partner_invoice_id': addr['invoice'],
            'partner_shipping_id': addr['delivery'],
            'payment_term': payment_term,
            # 'user_id': dedicated_salesman,
        }
        delivery_onchange = self.onchange_delivery_id(
            cr, uid, ids, False, part.id, addr['delivery'], False,
            context=context)
        val.update(delivery_onchange['value'])
        if pricelist:
            val['pricelist_id'] = pricelist
        if not self._get_default_section_id(
                cr, uid, context=context) and part.section_id:
            val['section_id'] = part.section_id.id
        sale_note = self.get_salenote(cr, uid, ids, part.id, context=context)
        if sale_note:
            val.update({'note': sale_note})
        val['section_id'] = part.section_id.id
        return {'value': val}


class SaleOrderLine(osv.osv):
    _inherit = 'sale.order.line'

    def onchange_product_uom(self, cursor, user, ids, pricelist, product, qty=0,
                             uom=False, qty_uos=0, uos=False, name='',
                             partner_id=False, lang=False, update_tax=True,
                             date_order=False, fiscal_position=False,
                             context=None):
        ctx = dict(context or {}, fiscal_position=fiscal_position)
        return self.product_uom_change(cursor, user, ids, pricelist, product,
                                       qty=qty, uom=uom, qty_uos=qty_uos,
                                       uos=uos, name=name,
                                       partner_id=partner_id, lang=lang,
                                       update_tax=update_tax,
                                       date_order=date_order, context=ctx)

    def _cart_update(self, cr, uid, ids, product_id=None, line_id=None,
                     add_qty=0, set_qty=0, context=None, **kwargs):
        """ Add or set product quantity, add_qty can be negative """
        sol = self.pool.get('sale.order.line')
        quantity = 0
        for so in self.browse(cr, uid, ids, context=context):
            if line_id:
                line_ids = so._cart_find_product_line(
                    product_id, line_id, context=context, **kwargs)
                if line_ids:
                    line_id = line_ids[0]
            # Create line if no line with product_id can be located
            if not line_id:
                values = self._website_product_id_change(
                    cr, uid, ids, so.id, product_id, qty=1, context=context)
                line_id = sol.create(cr, SUPERUSER_ID, values, context=context)
                if add_qty:
                    add_qty -= 1

            # compute new quantity
            if set_qty:
                quantity = set_qty
            elif add_qty != None:
                quantity = sol.browse(
                    cr, SUPERUSER_ID, line_id,
                    context=context).product_uom_qty + (
                                   add_qty or 0)

            # Remove zero of negative lines
            if quantity <= 0:
                sol.unlink(cr, SUPERUSER_ID, [line_id], context=context)
            else:
                # update line
                values = self._website_product_id_change(cr, uid, ids, so.id,
                                                         product_id,
                                                         qty=quantity,
                                                         line_id=line_id,
                                                         context=context)
                values['product_uom_qty'] = quantity

                sol.write(cr, SUPERUSER_ID, [line_id], values, context=context)

        return {'line_id': line_id, 'quantity': quantity}

    def _amount_line(self, cr, uid, ids, field_name, arg, context=None):
        tax_obj = self.pool.get('account.tax')
        cur_obj = self.pool.get('res.currency')
        res = {}
        if context is None:
            context = {}
        for line in self.browse(cr, uid, ids, context=context):
            price = line.price_unit * (1 - (line.discount or 0.0) / 100.0)
            taxes = tax_obj.compute_all(cr, uid, line.tax_id, price,
                                        line.product_uom_qty,
                                        line.additional_amount, line.product_id,
                                        line.order_id.partner_id)
            taxes['total'] = taxes[
                                 'total'] + line.additional_amount + line.fixed_amount
            cur = line.order_id.pricelist_id.currency_id
            res[line.id] = cur_obj.round(cr, uid, cur, taxes['total'])
        return res

    def _product_margin(self, cr, uid, ids, field_name, arg, context=None):
        cur_obj = self.pool.get('res.currency')
        res = {}
        for line in self.browse(cr, uid, ids, context=context):
            cur = line.order_id.pricelist_id.currency_id
            res[line.id] = 0
            if line.product_id:
                tmp_margin = line.price_subtotal - (
                        (line.purchase_price
                         or line.product_id.standard_price) * line.product_uom_qty)
                res[line.id] = cur_obj.round(cr, uid, cur, tmp_margin)
        return res

    _columns = {

        'name': fields.text('Description',
                            required=True,
                            readonly=False,
                            states={'draft': [('readonly', False)],
                                    'sent': [('readonly', False)],
                                    'progress': [('readonly', False)],
                                    'user_confirmed': [('readonly', False)],
                                    'done': [('readonly', False)],
                                    'manual': [('readonly', False)]}, ),
        'short_name': fields.text(
            'Short Description',
            readonly=True,
            states={'draft': [('readonly', False)],
                    'sent': [('readonly', False)],
                    'user_confirmed': [('readonly', False)],
                    'manual': [('readonly', False)]}, ),
        'line_article': fields.text('Line Article'),
        'product_id': fields.many2one(
            'product.product',
            'Product',
            domain=[('sale_ok', '=', True)],
            change_default=True, readonly=True,
            states={'draft': [('readonly', False)],
                    'sent': [('readonly', False)],
                    'user_confirmed': [('readonly', False)],
                    'manual': [('readonly', False)]},
            ondelete='restrict'),
        'price_unit': fields.float(
            'Unit Price',
            required=True,
            digits_compute=dp.get_precision(
                'Product Price'),
            readonly=False,
        ),
        'tax_id': fields.many2many(
            'account.tax',
            'sale_order_tax',
            'order_line_id',
            'tax_id',
            'Taxes',
            readonly=True,
            states={'draft': [('readonly', False)],
                    'sent': [('readonly', False)],
                    'user_confirmed': [('readonly', False)],
                    'manual': [('readonly', False)]},
            domain=['|', ('active', '=', False),
                    ('active', '=', True)]),
        'product_uom_qty': fields.float(
            'Quantity',
            digits_compute=dp.get_precision(
                'Product UoS'),
            required=True,
            readonly=True,
            states={'draft': [('readonly', False)],
                    'sent': [('readonly', False)],
                    'user_confirmed': [('readonly', False)],
                    'manual': [('readonly', False)]}, ),
        'product_uom': fields.many2one(
            'product.uom',
            'Unit of Measure ',
            required=True,
            readonly=True,
            states={'draft': [('readonly', False)],
                    'sent': [('readonly', False)],
                    'user_confirmed': [('readonly', False)],
                    'manual': [('readonly', False)]}, ),
        'product_uos_qty': fields.float(
            'Quantity (UoS)',
            digits_compute=dp.get_precision(
                'Product UoS'),
            readonly=True,
            states={'draft': [('readonly', False)],
                    'sent': [('readonly', False)],
                    'user_confirmed': [('readonly', False)],
                    'manual': [('readonly', False)]}, ),
        'discount': fields.float(
            'Discount (%)',
            digits_compute=dp.get_precision('Discount'),
            readonly=True,
            states={'draft': [('readonly', False)],
                    'sent': [('readonly', False)],
                    'user_confirmed': [('readonly', False)],
                    'manual': [('readonly', False)]}, ),
        'th_weight': fields.float(
            'Weight',
            readonly=True,
            states={'draft': [('readonly', False)],
                    'sent': [('readonly', False)],
                    'user_confirmed': [('readonly', False)],
                    'manual': [('readonly', False)]},
            digits_compute=dp.get_precision(
                'Stock Weight')),
        'state': fields.selection(
            [('cancel', 'Cancelled'),
             ('draft', 'Draft'),
             ('sent', 'Quotation Sent'),
             ('user_confirmed', 'User Confirmed'),
             ('manual', 'Sale to Invoice'),
             ('progress', 'Sales Order'),
             ('confirmed', 'Confirmed'),
             ('exception', 'Exception'),
             ('waiting_date', 'Waiting Schedule'),
             ('shipping_except', 'Shipping Exception'),
             ('invoice_except', 'Invoice Exception'),
             ('done', 'Done'),
             ],
            'Status', required=True, readonly=True, copy=False,
            help='* The \'Draft\' status is set when the related sales order in draft status. \
                      \n* The \'Confirmed\' status is set when the related sales order is confirmed. \
                      \n* The \'Exception\' status is set when the related sales order is set as exception. \
                      \n* The \'Done\' status is set when the sales order line has been picked. \
                      \n* The \'Cancelled\' status is set when a user cancel the sales order related.'),
        'delay': fields.float(
            'Delivery Lead Time', required=True,
            help="Number of days between the order confirmation and the shipping of the products to the customer",
            readonly=True,
            states={'draft': [('readonly', False)],
                    'sent': [('readonly', False)],
                    'user_confirmed': [('readonly', False)],
                    'manual': [('readonly', False)]}, ),

        'duplicate_unit_price': fields.float('Duplicate Unit Price'),
        'is_check_ems': fields.boolean('Check EMS ?'),
        'image2': fields.related('product_id',
                                 'image2',
                                 type='binary',
                                 string='Image2'),
        'image3': fields.related('product_id',
                                 'image3',
                                 type='binary',
                                 string='Image3'),
        'selected_image': fields.char("Selected Image"),
        'length_with_foam': fields.char('Length With Foam',
                                        translate=True,
                                        size=50),
        'length_without_foam': fields.char('Length Without Foam',
                                           translate=True,
                                           size=50),
        'additional_amount': fields.float(
            'Surcharge',
            help='Additional amount applied by pricelist rule.'),
        'price_subtotal': fields.function(
            _amount_line,
            string='Subtotal',
            digits_compute=dp.get_precision(
                'Account')),

        'fixed_amount': fields.float('Fixed Amount',
                                     digits_compute=dp.get_precision(
                                         'Product Price'),
                                     help='Specify the fixed amount to add amount calculated.'),

        'further_processing_ids': fields.many2many(
            "further.processing",
            'further_processing_line_rel',
            'further_processing_id',
            'line_id',
            string="Further Processing"),
        'e_low_open_groove': fields.boolean('Allow Open Groove Gluewire'),
        'gluewire': fields.boolean('Gluewire'),
        'color_laser_marking': fields.selection([('no', 'No'),
                                                 ('grey', 'Grey'),
                                                 ('white', 'White')],
                                                string='Color Laser Marking',
                                                default="no"),
        'print_logo': fields.boolean('Print Logo'),
        'logo_for_print': fields.binary('Logo For Print'),

        'margin': fields.function(_product_margin, string='Margin',
                                  digits_compute=dp.get_precision(
                                      'Product Price'),
                                  store=True),
    }

    _default = {
        'protection_foil': 0,
        'e_low': 0,
        'soundblasting': 0,
        'punching': 0,
    }

    def button_confirm(self, cr, uid, ids, context=None):
        return self.write(cr, uid, ids, {'state': 'sent'}, context=context)

    def create(self, cr, uid, values, context=None):
        line_id = super(SaleOrderLine, self).create(
            cr, uid, values, context=context)
        line = self.browse(cr, uid, line_id, context=context)
        ctx = context.copy()
        ctx['make_description'] = True
        line.create_description()
        return line_id

    def write(self, cr, uid, ids, values, context=None):
        if values.get('price_unit'):
            sale_order_pool = self.pool.get('sale.order')
            sale_line = self.browse(cr, uid, ids, context=context)
            if sale_line and sale_line.order_id.crm_lead_id:
                sale_order = sale_line.order_id
                default_sale_order_id = sale_order_pool.search(
                    cr, uid, [('crm_lead_id', '=', sale_order.crm_lead_id.id),
                              ('id', '!=', sale_order.id),
                              ('default_pricelist', '=', True)])
                if default_sale_order_id:
                    default_sale_order = sale_order_pool.browse(
                        request.cr, request.uid, default_sale_order_id,
                        context=request.context)
                    for d_line in default_sale_order.order_line:
                        if sale_line.product_id == d_line.product_id \
                                and sale_line.gluewire == d_line.gluewire \
                                and sale_line.color_laser_marking == \
                                d_line.color_laser_marking \
                                and sale_line.print_logo == d_line.print_logo \
                                and len(sale_line.further_processing_ids) == \
                                len(d_line.further_processing_ids):
                            check = True

                            for f in sale_line.further_processing_ids:
                                if f not in d_line.further_processing_ids:
                                    check = False
                            if check:
                                values['price_unit'] = d_line.price_unit
                                values['purchase_price'] = d_line.purchase_price

        return super(SaleOrderLine, self).write(
            cr, uid, ids, values, context=context)

    def unlink(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        """Allows to delete sales order lines in draft,cancel states"""
        for rec in self.browse(cr, uid, ids, context=context):
            if rec.state == 'user_confirmed':
                rec.write({'state': 'draft'})
        return super(SaleOrderLine, self).unlink(cr, uid, ids, context=context)


class sale_order_category(osv.osv):
    _name = 'sale.order.category'

    _columns = {
        'order_id': fields.many2one('sale.order', 'Order Id'),
        'category_id': fields.many2one('product.public.category',
                                       'Category Id'),
        'amount_untaxed': fields.float('Amount Untaxed'),
        'amount_tax': fields.float('Amount Tax'),
        'amount_total': fields.float('Amount Total'),
    }


sale_order_category()


class crm_lead(osv.osv):
    _inherit = 'crm.lead'
    _columns = {
        'attachment_id': fields.many2many('ir.attachment',
                                          'crm_class_ir_attachments_pdf_rel',
                                          'crm_class_id', 'crm_attachment_id',
                                          'Attached File'),
        'res_uid': fields.integer('Resource uid'),
        'dxf_image': fields.binary('DXF Image'),
        'dxf_filename': fields.char('DXF filename'),
    }


crm_lead()
