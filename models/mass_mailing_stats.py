# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Business Applications
#    Copyright (c) 2012 OpenERP S.A. <http://openerp.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from openerp.osv import osv, fields


class MailMailStatistics(osv.osv):
    _inherit = 'mail.mail.statistics'
    _columns = {
        'partner_name': fields.many2one('res.partner', 'Partner'),
        'type': fields.selection([
            ('email', 'Email'),
            ('mass_mailing', 'Mass Mailing'),
        ],
            string='Type',
            help="Defines whether this email was a normal email or through mass mailing"),
    }

    def create(self, cr, uid, vals, context=None):
        # partner_id = vals['res_id']
        # partner_name = self.pool.get('res.partner').browse(
        #     cr, uid, [partner_id], context=context)
        # vals['partner_name'] = partner_name.id

        if 'type' not in vals:
            vals['type'] = 'mass_mailing'
        res = super(MailMailStatistics, self).create(
            cr, uid, vals, context=context)
        return res


class MailMail(osv.Model):
    _inherit = 'mail.mail'

    _columns = {
        'to_track': fields.boolean(string='Track Mail ?',
                                   help="By unchecking the 'To track' "
                                        "field, you will be able to track "
                                        "the email sent."),
    }

    def create(self, cr, uid, values, context=None):
        # notification field: if not set, set if mail
        # comes from an existing mail.message
        if 'notification' not in values and values.get('mail_message_id'):
            values['notification'] = True
        if 'model' in values:
            if values['model'] == False and values['to_track']:
                recipients = values['recipient_ids'][0][2]
                for recipient in recipients:
                    values['recipient_ids'] = [[6, False, [recipient]]]
                    mail_id = super(MailMail, self).create(
                        cr, uid, values, context=context)
                    vals = {}

                    vals['partner_name'] = recipient
                    vals['model'] = 'res.partner'
                    vals['mail_mail_id'] = mail_id
                    vals['res_id'] = recipient
                    vals['type'] = 'email'

                    self.pool.get('mail.mail.statistics').create(
                        cr, uid, vals, context=None)
            else:
                return super(MailMail, self).create(
                    cr, uid, values, context=context)
        return super(MailMail, self).create(
            cr, uid, values, context=context)
