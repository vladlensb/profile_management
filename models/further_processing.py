# -*- coding: UTF-8 -*-

##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2016-Today Libre Comunication (<erpsystem.com.ua@gmail.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp import fields, models, api


class FurtherProcessing(models.Model):
    _name = "further.processing"

    name = fields.Char(string="Name", translate=True)
    identifier = fields.Char(string="Identifier")
    google_sequence = fields.Integer(string="Google Identifier")
    processing_article_number = fields.Char('Processing Article Number')
    active = fields.Boolean(string='Active')

    _sql_constraints = [
        ('google_sequence_unique',
         'unique(google_sequence)',
         'You can not have two identical Google Identifier')
    ]
