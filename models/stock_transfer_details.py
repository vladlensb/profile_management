# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-TODAY OpenERP S.A. <http://www.odoo.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from datetime import datetime
from openerp import models, fields, api, _
import logging
from openerp.exceptions import ValidationError

_logger = logging.getLogger(__name__)


class StockTransferDetails(models.TransientModel):
    _inherit = 'stock.transfer_details'

    @api.one
    def do_detailed_transfer(self):
        picking_ids = []

        picking_type = self.env['stock.picking.type'].search(
            [('code','=','incoming')])

        if self.picking_id.picking_type_id not in picking_type:
            if self.picking_id.sale_id:
                try:
                    sale_id = self.picking_id.sale_id
                    purchase_line_ids = sale_id.purchase_line_ids
                    picking_ids = purchase_line_ids[0].order_id.picking_ids
                except:
                    raise ValidationError(
                        _('You do not have the confirm purchase'))
            product_category = self.env['product.category'].search(
                [('name', 'in', ('Wooden Box', 'Steel Box', 'Service Units'))])
            if picking_ids and self.picking_id not in picking_ids:
                check = True
                purchase_picking_ids = [picking
                                        for picking in picking_ids
                                        if picking.state == 'done']
                sale_picking_ids = [
                    picking for picking in
                    self.picking_id.sale_id.picking_ids
                    if picking.state == 'done']
                wrong_item = []
                for item in self.item_ids:
                    if item.product_id.categ_id not in product_category:
                        purchase_amount = 0
                        for picking in purchase_picking_ids:
                            for line in picking.move_lines:
                                if line.product_id == item.product_id:
                                    purchase_amount += line.product_uom_qty
                        sale_amount = 0
                        for picking in sale_picking_ids:
                            for line in picking.move_lines:
                                if line.product_id == item.product_id:
                                    sale_amount += line.product_uom_qty
                        if purchase_amount < sale_amount + item.quantity:
                            if item not in wrong_item:
                                wrong_item.append(item)
                            check = False

                # if not check:
                #     wrong_product = ',\n'.join(
                #         i.product_id.name for i in wrong_item)
                #     raise ValidationError(_('You do not have the required '
                #                             'amount of product: %s. '
                #                             'Make a purchase' % wrong_product))
        return super(StockTransferDetails, self).do_detailed_transfer()
