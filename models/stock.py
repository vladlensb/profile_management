# -*- coding: UTF-8 -*-

##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2016-Today Libre Comunication (<erpsystem.com.ua@gmail.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


from openerp.osv import osv, fields
import openerp.addons.decimal_precision as dp


class AccountInvoice(osv.osv):
    _inherit = "account.invoice"

    _columns = {
        'weight': fields.float(string='Weight'),
    }


class StockPicking(osv.osv):
    _inherit = "stock.picking"

    def action_invoice_create(self, cr, uid, ids, journal_id, group=False,
                              type='out_invoice', context=None):
        """ Creates invoice based on the invoice state selected for picking.
        @param journal_id: Id of journal
        @param group: Whether to create a group invoice or not
        @param type: Type invoice to be created
        @return: Ids of created invoices for the pickings
        """
        context = context or {}
        todo = {}
        picking = None
        for picking in self.browse(cr, uid, ids, context=context):
            partner = self._get_partner_to_invoice(cr, uid, picking,
                                                   dict(context, type=type))
            # grouping is based on the invoiced partner
            if group:
                key = partner
            else:
                key = picking.id
            for move in picking.move_lines:
                if move.invoice_state == '2binvoiced':
                    if (move.state != 'cancel') and not move.scrapped:
                        todo.setdefault(key, [])
                        todo[key].append(move)
        invoices = []
        for moves in todo.values():
            invoices += self._invoice_create_line(cr, uid, moves, journal_id,
                                                  type, context=context)

        if invoices and picking:
            self.pool.get('account.invoice').write(
                cr, uid, invoices, {'weight': picking.weight}, context=context)
        return invoices


class stock_move(osv.osv):
    _inherit = 'stock.move'

    def _cal_move_weight(self, cr, uid, ids, name, args, context=None):
        res = {}
        for move in self.browse(cr, uid, ids, context=context):
            weight = weight_net = 0.00
            if move.product_id.weight > 0.00:

                converted_qty = move.product_qty
                weight = (converted_qty * move.product_id.weight)

                if move.product_id.weight_net > 0.00:
                    weight_net = (converted_qty * move.product_id.weight_net)

                if move.procurement_id and move.procurement_id.sale_line_id \
                        and move.procurement_id.sale_line_id.gluewire:
                    weight = (converted_qty * (
                        move.product_id.weight + 0.0002))
                    if move.product_id.weight_net > 0.00:
                        weight_net = (converted_qty * (
                            move.product_id.weight_net + 0.0002))

                elif move.purchase_line_id \
                        and move.purchase_line_id.sale_order_line_id \
                        and move.purchase_line_id.sale_order_line_id.gluewire:
                    weight = (converted_qty * (
                        move.product_id.weight + 0.0002))
                    if move.product_id.weight_net > 0.00:
                        weight_net = (converted_qty * (
                            move.product_id.weight_net + 0.0002))

            res[move.id] = {
                'weight': weight,
                'weight_net': weight_net,
            }
        return res

    _columns = {
        'weight': fields.function(
            _cal_move_weight,
            type='float',
            string='Weight',
            digits_compute=dp.get_precision('Stock Weight'),
            multi='_cal_move_weight',
            store={'stock.move': (lambda self, cr, uid, ids, c=None: ids,
                                  ['product_id', 'product_uom_qty',
                                   'product_uom'], 30)}),
        'weight_net': fields.function(
            _cal_move_weight,
            type='float',
            string='Net weight',
            digits_compute=dp.get_precision('Stock Weight'),
            multi='_cal_move_weight',
            store={'stock.move': (lambda self, cr, uid, ids, c=None: ids,
                                  ['product_id', 'product_uom_qty',
                                   'product_uom'], 30)}),
        'sale_order_line_id': fields.many2one('sale.order.line',
                                              string="Sale Order Line")
    }
