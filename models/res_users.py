# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Business Applications
#    Copyright (c) 2012 OpenERP S.A. <http://openerp.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import logging
import base64
#import numpy as np
import urllib
import werkzeug.urls
import urlparse
import simplejson
import json
import urllib, urllib2
import oauth2 as oauth
from openerp.api import Environment
from ast import literal_eval
from openerp.addons.web.http import request

import openerp
from openerp import tools, api
from openerp.addons.auth_signup.res_users import SignupError
from openerp.osv import osv, fields
from openerp import SUPERUSER_ID
from datetime import datetime, timedelta
from openerp.tools.misc import DEFAULT_SERVER_DATETIME_FORMAT, ustr
from openerp.addons.thermevo_website_management.controllers.therm_login import ThermevoRegister
from urlparse import urljoin
_logger = logging.getLogger(__name__)


def now(**kwargs):
    dt = datetime.now() + timedelta(**kwargs)
    return dt.strftime(DEFAULT_SERVER_DATETIME_FORMAT)


class ResUsers(osv.Model):
    _inherit = 'res.users'

    def default_get(self, cr, uid, fields, context=None):
        values = super(ResUsers, self).default_get(cr, uid, fields, context)
        group_id = self.pool.get('res.groups').search(
            cr, uid, [('name', '=', 'Blog Content Editor')], context=context)
        groups_ids = values['groups_id']
        groups_ids[0][-1].append(group_id[0])
        values['groups_id'] = groups_ids
        return values

    def create(self, cr, uid, vals, context=None):
        env = Environment(cr, uid, context)
        res_obj = env['res.groups']
        res_data = res_obj.search([['name', '=', 'Profile Management']])
        res_data_id=res_obj.browse([int(res_data[0])])
        group_name = "in_group_" + str(res_data.id)
        blog_data = res_obj.search([['name', '=', 'Blog Content Editor']])
        blog_data_id=res_obj.browse([int(blog_data[0])])
        blog_group = "in_group_" + str(blog_data.id)
        vals[group_name] = True
        vals[blog_group] = True
        img_path = openerp.modules.get_module_resource(
            'profile_management', 'static/src/img', 'noname_user_photo.png')
        with open(img_path, 'rb') as f:
            image = f.read()
        vals.update({'image':tools.image_resize_image_big(
            image.encode('base64'))})
        if 'groups_id' in vals:
            vals.update(
                groups_id=[(4, int(blog_data_id.id)), (4, int(res_data_id.id))])
        new_id = super(ResUsers, self).create(cr, uid, vals, context)
        if 'oauth_provider_id' in vals:
            if vals['oauth_provider_id']:
                self.action_send_welcome_email(cr, uid, new_id, context)
        return new_id

    def signup(self, cr, uid, values, token=None, context=None):
        """ signup a user, to either:
            - create a new user (no token), or
            - create a user for a partner (with token, but no user for partner), or
            - change the password of a user (with token, and existing user).
            :param values: a dictionary with field values that are written on user
            :param token: signup token (optional)
            :return: (dbname, login, password) for the signed up user
        """
        if token:
            # signup with a token: find the corresponding partner id
            res_partner = self.pool.get('res.partner')
            partner = res_partner._signup_retrieve_partner(
                cr, uid, token, check_validity=True,
                raise_exception=True, context=None)
            # invalidate signup token
            partner.write(
                {'signup_token': False,
                 'signup_type': False,
                 'signup_expiration': False})

            partner_user = partner.user_ids and partner.user_ids[0] or False

            # avoid overwriting existing (presumably correct)
            # values with geolocation data
            if partner.country_id or partner.zip or partner.city:
                values.pop('city', None)
                values.pop('country_id', None)
            if partner.lang:
                values.pop('lang', None)

            if partner_user:
                # user exists, modify it according to values
                values.pop('login', None)
                values.pop('name', None)
                partner_user.write(values)
                return (cr.dbname, partner_user.login, values.get('password'))
            else:
                # user does not exist: sign up invited user
                values.update({
                    'name': partner.name,
                    'partner_id': partner.id,
                    'email': values.get('email') or values.get('login'),
                })
                if partner.company_id:
                    values['company_id'] = partner.company_id.id
                    values['company_ids'] = [(6, 0, [partner.company_id.id])]
                self._signup_create_user(cr, uid, values, context=context)
        else:
            # no token, sign up an external user
            values['email'] = values.get('email') or values.get('login')
            self._signup_create_user(cr, uid, values, context=context)

        return (cr.dbname, values.get('login'), values.get('password'))

    def _signup_create_user(self, cr, uid, values, context=None):
        """ create a new user from the template user """
        ir_config_parameter = self.pool.get('ir.config_parameter')
        template_user_id = literal_eval(ir_config_parameter.get_param(
            cr, uid, 'auth_signup.template_user_id', 'False'))
        assert template_user_id and self.exists(
            cr, uid, template_user_id, context=context), \
            'Signup: invalid template user'

        # check that uninvited users may sign up
        if 'partner_id' not in values:
            if not literal_eval(ir_config_parameter.get_param(
                    cr, uid, 'auth_signup.allow_uninvited', 'False')):
                raise SignupError('Signup is not allowed for uninvited users')

        assert values.get('login'), "Signup: no login given for new user"
        assert values.get('partner_id') or values.get('name'),\
            "Signup: no name or partner given for new user"

        # create a copy of the template user
        # (attached to a specific partner_id if given)
        if values.get('oauth_provider_id'):
            values['active'] = True
        else:
            values['active'] = False
        context = dict(context or {}, no_reset_password=True)
        try:
            with cr.savepoint():
                return self.copy(
                    cr, uid, template_user_id, values, context=context)
        except Exception, e:
            # copy may failed if asked login is not available.
            raise SignupError(ustr(e))

    def _auth_oauth_rpc(self, cr, uid, endpoint, access_token, context=None):
        params = werkzeug.url_encode({'access_token': access_token})
        if urlparse.urlparse(endpoint)[4]:
            url = endpoint + '&' + params
        else:
            url = endpoint + '?' + params
        provider = self.pool.get('auth.oauth.provider').search(
            cr, uid,[('validation_endpoint', '=', endpoint)])
        provider_id = self.pool.get('auth.oauth.provider').browse(
            cr, uid, provider, context=context)
        if provider_id['name'].upper() == "LINKEDIN":#Check for Linked IN
            url = url + "&format=json"
            req = urllib2.Request(url)
            # Added header for get profile data from Linkedin.
            req.add_header('Authorization',access_token)
            response = urllib2.urlopen(req)
            response_json = response.read()
            print "\n\n\nLinkedIN Returned JSON is ===> ",response_json
            return simplejson.loads(response_json)
        elif provider_id['name'].upper() == "FACEBOOK":#Check for Facebook
            f = urllib2.urlopen(url)
            response = f.read()
            newResponse = simplejson.loads(response)
            if 'user_id' in newResponse.keys():
                newResponse = newResponse
            elif 'id' in newResponse.keys():
                newResponse['user_id']=newResponse['id']
            print "\n\n\nFacebook Returned JSON is ===> ",newResponse
            return newResponse
        elif provider_id['name'].upper() == "TWITTER":#Check for Twitter
            token = oauth.Token(
                context['params']['params']['oauth_token'],
                context['params']['params']['request_token']['oauth_token_secret'])
            token.set_verifier(context['params']['params']['oauth_verifier'])
            client = oauth.Client(
                context['params']['params']['consumer'], token)
            resp, content = client.request(
                'https://api.twitter.com/oauth/access_token', "POST")
            access_token = dict(urlparse.parse_qsl(content))
            # Save the output of the script which gives the access token
            print access_token.keys()
            print access_token.values()

            client=oauth.Client(context['params']['params']['consumer'])

            url_full = 'https://api.twitter.com/1.1/statuses/user_timeline.json?screen_name=%s&count=2' % access_token['screen_name']
            resp1, content1 = client.request(url_full, "GET")
            if resp1['status'] != '200':
                raise Exception("Invalid response %s." % resp1['status'])

            access_token["data"]=content1
            return access_token

        else:
            f_google = urllib2.urlopen(url)
            response_google = f_google.read()
            json_data = simplejson.loads(response_google)
            print "\n\n\nResponse in Other=====> ",json_data
            #jsonData = simplejson.loads(response_google);
            #print "\n\n\nOther Returned JSON is ===> ",jsonData
            return json_data
    def url_to_image(url):
        # download the image, convert it to a NumPy array, and then read
        # it into OpenCV format
        resp = urllib.urlopen(url)
        image = np.asarray(bytearray(resp.read()), dtype="uint8")

        # return the image
        return image
    def _auth_oauth_validate(self, cr, uid, provider, access_token, context=None):
        """ return the validation data corresponding to the access token """
        p = self.pool.get('auth.oauth.provider').browse(
            cr, uid, provider, context=context)
        if p['name'].upper() == "LINKEDIN":#Check for Linked IN
            validation = self._auth_oauth_rpc(cr, uid, p.validation_endpoint,"Bearer "+access_token)
        elif p['name'].upper() == "TWITTER":
            validation = self._auth_oauth_rpc(cr, uid, p.validation_endpoint,"Bearer "+access_token,{"params":context})
            return validation
        else:
            validation = self._auth_oauth_rpc(cr, uid, p.validation_endpoint, access_token)

        if validation.get("error"):
            raise Exception(validation['error'])
        if p.data_endpoint:
            if p['name'].upper() == "LINKEDIN":
                data = self._auth_oauth_rpc(cr, uid, p.data_endpoint, "Bearer "+access_token)
            else:
                data = self._auth_oauth_rpc(cr, uid, p.validation_endpoint, access_token)

            validation.update(data)
        return validation
    def _auth_oauth_signin(self, cr, uid, provider, validation, params, context=None):
        """ retrieve and sign in the user corresponding to provider and validated access token
            :param provider: oauth provider id (int)
            :param validation: result of validation of access token (dict)
            :param params: oauth parameters (dict)
            :return: user login (str)
            :raise: openerp.exceptions.AccessDenied if signin failed

            This method can be overridden to add alternative signin methods.
        """
        #print "Getting final validation in _auth_oauth_signin ====>>> ",validation["data"]

        try:
            oauth_uid = validation['user_id']
            user_ids = self.search(cr, uid, [("oauth_uid", "=", oauth_uid), ('oauth_provider_id', '=', provider)])

            z = self.pool.get('auth.oauth.provider').browse(cr, uid, provider, context=context)
            is_current = 'None'
            job_title = 'None'
            company_name = 'None'
            company_type = 'None'
            industry = 'None'
            c_id = 'None'
            company_ticker = 'None'
            position = 'None'
            company_ticker = 'None'
            numConnections = 'None'
            location = 'None'
            country_code = 'None'
            current_share_title = 'None'
            current_share_description = 'None'

            if z['name'].upper() == "LINKEDIN":
                if 'positions' in validation:
                    if 'values' in  validation['positions']:
                        is_current = validation['positions']['values'][0]['isCurrent']
                        job_title = validation['positions']['values'][0]['title']
                        company_name = validation['positions']['values'][0]['company']['name']
                        try:
                            if 'type' in validation['positions']['values'][0]['company']:
                                company_type = validation['positions']['values'][0]['company']['type']
                        except KeyError:
                            company_type = 'None'
                        if 'industry' in validation['positions']['values'][0]['company']:
                            industry = validation['positions']['values'][0]['company']['industry']
                        if 'id' in validation['positions']['values'][0]['company']:
                            c_id = validation['positions']['values'][0]['company']['id']
                        if "ticker" in validation['positions']['values'][0]['company']:
                            company_ticker = validation['positions']['values'][0]['company']['ticker']
                            print "Company Ticker ==> ",company_ticker
                position = validation['headline']

                numConnections = validation['numConnections']
                location = validation['location']['name']
                country_code = validation['location']['country']['code']
                if "currentShare" in validation:
                    current_share_title = (
                        validation['currentShare']['content']['title'])
                    current_share_description = (
                        validation['currentShare']['content']['description'])

            if not user_ids:
                raise openerp.exceptions.AccessDenied()
            assert len(user_ids) == 1
            user = self.browse(cr, uid, user_ids[0], context=context)
            user.write({'oauth_access_token': params['access_token']})
            return user.login
        except openerp.exceptions.AccessDenied, access_denied_exception:
            if context and context.get('no_user_creation'):
                return None
            p = self.pool.get('auth.oauth.provider').browse(cr, uid, provider, context=context)

            oauth_uid = validation['user_id']
            email = validation.get('email', 'provider_%s_user_%s' % (provider, oauth_uid))
            token = None
            if p['name'].upper() == "LINKEDIN":
                email = validation['emailAddress']
                name = validation["firstName"]
                last_name = validation["lastName"]
                if 'pictureUrls' in validation:
                    picture_url = validation["pictureUrls"]
                    if 'values' in picture_url:
                        url_link = picture_url.get('values')[0]
                        response = urllib.urlopen(url_link)
                        binaryimage = response.read()
                        image = base64.b64encode(binaryimage)
                token = None
            elif p['name'].upper() == "TWITTER":
                test = dict(urlparse.parse_qsl(validation["data"]))
                python_obj = json.loads(validation["data"])
                print "python_obj===>>", len(python_obj)

                #resp = urllib.urlopen(python_obj[0]["user"]["profile_image_url"])
                #image = np.asarray(bytearray(resp.read()), dtype="uint8")

                email = validation['screen_name']
                if len(python_obj) > 0:
                    name = python_obj[0]["user"]["name"]
                else:
                    name = validation['screen_name']
                token = None
            else:
                name = validation.get('name', email)

            if p['name'].upper() == "LINKEDIN":
                values = {
                    'name': name,
                    'last_name':last_name,
                    'login': email,
                    'email': email,
                    'oauth_provider_id': provider,
                    'oauth_uid': oauth_uid,
                    'oauth_access_token': params['access_token'],
                    'active': True,
                    'position': position,
                    'job_title': job_title,
                    'is_current': is_current,
                    'location': location,
                    'company_name': company_name,
                    'country_code': country_code,
                    'current_share_title': current_share_title,
                    'current_share_description': current_share_description,
                    'numConnections': numConnections,
                    'company_type': company_type,
                    'company_ticker': company_ticker,
                    'industry': industry,
                    'c_id': c_id,
                }
                if 'pictureUrls' in validation:
                    picture_url = validation["pictureUrls"]
                    if 'values' in picture_url:
                        values.update({
                            'image':image,
                        })
            else:
                values = {
                    'name': name,
                    'login': email,
                    'email': email,
                    'oauth_provider_id': provider,
                    'oauth_uid': oauth_uid,
                    'oauth_access_token': params['access_token'],
                    'active': True,
                }
            print "Data in Value=====> ",values
            try:
                _, login, _ = self.signup(cr, uid, values, token, context=context)
                if p['name']=="LinkedIN":
                    #-----Only LinkedIn Users are added to Acees Right 2 gruop--------
                    res_users_search=self.pool.get('res.users').search(cr,uid,[('login','=',login)],context=context)
                    res_group=self.pool.get('res.groups')
                    group_id_search=res_group.search(cr,uid,[('name','=','Access Right 2')],context=context)
                    res_group.write(cr, uid,group_id_search, {'users': [(4,user) for user in res_users_search]}, context=context)
                    return login
                else:
                    return login
            except SignupError:
                print "\n\n\n\n\nError in Sighup................"
                raise access_denied_exception

    def auth_oauth(self, cr, uid, provider, params, context=None):
        # Advice by Google (to avoid Confused Deputy Problem)
        # if validation.audience != OUR_CLIENT_ID:
        #   abort()
        # else:
        #   continue with the process
        access_token = params.get('access_token')
        validation = self._auth_oauth_validate(cr, uid, provider, access_token,{"params":params})
        # required check
        p = self.pool.get('auth.oauth.provider').browse(cr, uid, provider, context=context)
        if p['name'].upper() == "LINKEDIN":
            validation["user_id"]=validation["id"]
        if not validation.get('user_id'):
            raise openerp.exceptions.AccessDenied()
        # retrieve and sign in user
        login = self._auth_oauth_signin(cr, uid, provider, validation, params, context=context)
        if not login:
            raise openerp.exceptions.AccessDenied()
        # return user credentials
        return (cr.dbname, login, access_token)

    def user_varification_mail(self, cr, uid, login, context=None):
        """ retrieve the user corresponding to login (login or email),
            and reset their password
        """
        # user_ids = self.search(cr, uid, [('login', '=', login)], context=context)
        user_ids=self.pool.get('res.users').search(cr,uid,[('login','=',login),('active','=',False)],context=context)
        if not user_ids:
            user_ids=self.pool.get('res.users').search(cr,uid,[('login','=',login)],context=context)
        if len(user_ids) != 1:
            raise Exception('invalid username or email')
        return self.action_send_verification_email(cr, uid, user_ids, context=context)

    def action_send_verification_email(self, cr, uid, ids, context=None):
        """ create signup token for each user, and send their signup url by email """
        # prepare reset password signup
        res_partner = self.pool.get('res.partner')
        partner_ids = [user.partner_id.id for user in self.browse(cr, uid, ids, context)]
        res_partner.signup_prepare(cr, uid, partner_ids, signup_type="reset", expiration=now(days=+1), context=context)

        if not context:
            context = {}

        # send email to users with their signup url
        template = False
        print "\n\n\n context========>", context
        if context.get('create_user'):
            try:
                # get_object() raises ValueError if record does not exist
                print "\n\n\n =========create mail function called========="
                template = self.pool.get('ir.model.data').get_object(cr, uid, 'thermevo_website_management', 'set_verification_email')
            except ValueError:
                pass
        if not bool(template):
            print "\n\n\n =========reset password mail function called========="
            template = self.pool.get('ir.model.data').get_object(cr, uid, 'thermevo_website_management', 'set_verification_email')
        assert template._name == 'email.template'

        for user in self.browse(cr, uid, ids, context):
            if not user.email:
                raise osv.except_osv(_("Cannot send email: user has no email address."), user.name)
            self.pool.get('email.template').send_mail(cr, uid, template.id, user.id, force_send=True, raise_exception=True, context=context)

    def _get_signup_url_for_confirm_action(self, cr, uid, ids, action=None, view_type=None, menu_id=None, res_id=None, model=None, context=None):
        """ generate a signup url for the given partner ids and action, possibly overriding
            the url state components (menu_id, id, view_type) """

        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry
        print"--------------------------In confirm url---------------------------------------------------------"
        if context is None:
            context= {}
        res = dict.fromkeys(ids, False)
        base_url = self.pool.get('ir.config_parameter').get_param(cr, uid, 'web.base.url')

        new_user_id = ids[-1]
        res_user_obj = pool.get('res.users')
        res_users_search= res_user_obj.search(cr,uid,[('active','=',False),('id','=',new_user_id)],context=context)
        user = res_user_obj.browse(cr, SUPERUSER_ID, res_users_search, context=context)
        user_login = user.login

        for partner in self.browse(cr, uid, ids, context):
            # when required, make sure the partner has a valid signup token
            if context.get('signup_valid') and not partner.user_ids:
                self.signup_prepare(cr, uid, [partner.id], context=context)

            route = 'login'
            # the parameters to encode for the query
            query = dict(db=cr.dbname,login=user_login)
            signup_type = context.get('signup_force_type_in_url', partner.signup_type or '')
            if signup_type:
                route = 'login' if signup_type == 'reset' else signup_type

            if partner.signup_token and signup_type:
                query['token'] = partner.signup_token
            elif partner.user_ids:
                query['login'] = partner.user_ids[0].login
            else:
                continue        # no signup token, no user, thus no signup url!

            fragment = dict()
            if action:
                fragment['action'] = action
            if view_type:
                fragment['view_type'] = view_type
            if menu_id:
                fragment['menu_id'] = menu_id
            if model:
                fragment['model'] = model
            if res_id:
                fragment['id'] = res_id

            if fragment:
                query['redirect'] = '/page#' + werkzeug.url_encode(fragment)

            res[partner.id] = urljoin(base_url, "/page/%s?%s" % (route, werkzeug.url_encode(query)))
        return res

    def _get_signup_confirm_url(self, cr, uid, ids, name, arg, context=None):
        print"----------------------Get sign up method-------------------------------------"
        # print"--------------------------------------------------------",kwargs
        # # for partner in self.browse(cr, uid, ids, context):
        # #     print'--------------------------------------------------partner-----------------',partner
        # kwargs = {'last_name': u'', 'login': u'cbaraiya@surekhatech.com', 'password': u'test', 'name': u'test'}
        #
        # args =()
        #
        # response = super(ThermevoRegister,self).web_login(args,kwargs)
        #
        #
        # print"---------------------------------respone-----------------------",response
        return self._get_signup_url_for_confirm_action(cr, uid, ids, context=context)

    def action_send_welcome_email(self, cr, uid, ids, context=None):
        if not context:
            context = {}

        template = False
        if context.get('create_user'):
            try:
                template = self.pool.get('ir.model.data').get_object(cr, uid, 'thermevo_website_management', 'set_welcome_email')
            except ValueError:
                pass
        if not bool(template):
            template = self.pool.get('ir.model.data').get_object(cr, uid, 'thermevo_website_management', 'set_welcome_email')
        assert template._name == 'email.template'

        for user in self.browse(cr, uid, ids, context):
            if not user.email:
                raise osv.except_osv(_("Cannot send email: user has no email address."), user.name)
            self.pool.get('email.template').send_mail(cr, uid, template.id, user.id, force_send=True, raise_exception=True, context=context)

    _columns ={
        'signup_confirm_url': fields.function(_get_signup_confirm_url, type='char', string='Signup confirm URL'),
        'is_sale_person': fields.boolean('Default Sale Person'),
        # 'test_user': fields.boolean('Thermevo Test User'),
        # 'test_manager': fields.boolean('Thermevo Test Manager'),
    }

    #-----------------new model for user settings for notification and signature-------------------

class user_setting(osv.Model):
    _name = "user.setting"
    _columns = {
        'res_id':fields.integer('User ID'),
        'first_name':fields.boolean('First Name'),
        'last_name':fields.boolean('Last Name'),
        'company_name':fields.boolean('Company Name'),
        'new_post_notice':fields.boolean('New Post Notice',help='Receive notices of new posts in Blog'),
        'new_comment_notice':fields.boolean('New Comment Notice',help='Receive notices of comments about my article'),
        'already_comment_notice':fields.boolean('Already Comment Notice', help='Receive notices of new comments about the artcle which I have commented'),
    }