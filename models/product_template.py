# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Business Applications
#    Copyright (c) 2012 OpenERP S.A. <http://openerp.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from openerp.osv import osv, fields
from openerp import api
from openerp.tools.translate import _
import logging
import math

_logger = logging.getLogger(__name__)


def search_geometric(product):
    geometric = '0'
    area = '0'
    default_code_child = None
    for child in product.product_variant_ids:
        if not default_code_child \
                and child.default_code \
                and child.fixed_article:
            default_code_child = child.default_code
            default_code_dict = child.default_code.split('.')
            if len(default_code_dict) > 1:
                default_code_child = default_code_dict[0]
    if default_code_child and len(default_code_child) == 7:
        d = str(default_code_child)
        geometric = d[1]
        try:
            area = d[4] + d[5] + d[6]
        except:
            try:
                area = d[4] + d[5]
            except:
                area = '0'
    return geometric, area


class ProductProduct(osv.osv):
    _inherit = 'product.product'
    _columns = {
        'length': fields.float(string='Length'),
        'gluewire': fields.integer(string='Gluewire'),
        'foam': fields.boolean(string='Foam'),
        'minimum_amount': fields.integer(string='Minimum Amount'),
        'fixed_amount': fields.float(string='Fixed Amount'),
        'fixed_article': fields.boolean(string='Article is Fixed'),

    }
    _defaults = {
        'minimum_amount': 5000,
        'length': 6.0,
        'gluewire': 0,
        'foam': False
    }

    def create_second_name(self, cr, uid, ids, context=None):
        product_product = self.browse(cr, uid, ids, context=context)
        for product in product_product:
            if product.product_tmpl_id.owned_by == 'Customer':
                crm_lead = product.product_tmpl_id.lead_id
                second_name = 'Insulating Strip THERMEVO EvoBreak ' \
                              '%s, %s mm x %s mm, %s m' % (
                                  product.material_id.name,
                                  product.product_element_width,
                                  product.product_element_height,
                                  str(product.length))
                if crm_lead:
                    try:
                        dxf_filename = crm_lead.dxf_filename.replace('.input',
                                                                     '')
                        second_name += ', cust. ref# %s' % dxf_filename
                    except:
                        pass
            else:

                second_name = 'Insulating Strip THERMEVO EvoBreak %s, %s, ' \
                              '%s mm x %s mm, %s m' % (
                                  product.material_id.name,
                                  product.product_article,
                                  product.product_element_width,
                                  product.product_element_height,
                                  str(product.length))
            product.write({'second_name': second_name})

    def create_article(self, cr, uid, ids, context=None):
        product_product = self.browse(cr, uid, ids, context=context)
        for product in product_product:
            material = product.material_id.material_article_number
            if not material:
                material = '0'
            article_width = str(math.floor(product.element_width))
            try:
                article_width = article_width.split('.')[0]
            except:
                pass
            geometric, new_article_square = search_geometric(
                product.product_tmpl_id)
            if new_article_square == '0':
                article_square = str(
                    round(math.sqrt(product.product_tmpl_id.square), 2))
                article_square = article_square.split('.')
                new_article_square = '%s%s' % (
                    article_square[0], article_square[1])
                if len(article_square[1]) == 1:
                    new_article_square += '0'
            product_article = '%s%s%s%s.00000' % (
                str(material), geometric, article_width, new_article_square)

            if product.fixed_article:
                old_default_code = product.default_code
                default_code_dict = old_default_code.split('/')
                if len(default_code_dict) > 1:
                    static_default_code = default_code_dict[0].split('.')[0]
                    product_article = static_default_code + '.00000'

            default_code = product_article + '/%s' % str(product.length)

            if product.owned_by == 'Customer':
                product_article += '/%s/%s' % (
                    str(product.length), str('%04d') % uid)
            self.write(cr, uid, product.id,
                       {'article': product_article,
                        'default_code': default_code}, context=context)

    def create(self, cr, uid, values, context=None):
        product_id = super(ProductProduct, self).create(
            cr, uid, values, context=context)
        if not values.get('article'):
            self.create_article(cr, uid, product_id, context=context)
        return product_id

    def write(self, cr, uid, ids, values, context=None):
        if 'product_tmpl_id' in values.keys() \
                and not values.get('product_tmpl_id'):
            del values['product_tmpl_id']
        res = super(ProductProduct, self).write(
            cr, uid, ids, values, context=context)
        if not values.get('article'):
            for product in self.browse(cr, uid, ids, context=context):
                self.create_article(cr, uid, product.id, context=context)
        return res

    def create_description_sale(self, cr, uid, ids, context=None):
        description_sale = u''
        destctiption_template = u'Q-ty in pallet: %s Pcs x %s m = %s m\n'
        product = self.browse(cr, uid, ids, context=context)
        if product.qty_in_pallet and product.length:
            result = product.length * product.qty_in_pallet
            description_sale = destctiption_template % (
                product.qty_in_pallet, product.length, result)
        return description_sale


class ProductTemplate(osv.osv):
    _inherit = 'product.template'

    _columns = {
        'minimum_width_profile': fields.integer(string='Minimum Width'),
        'maximum_width_profile': fields.integer(string='Maximum Width'),
        'mimimum_height_profile': fields.integer(string='Minimum Height'),
        'maximum_height_profile': fields.integer(string='Maximum Height'),
        'image1': fields.binary(
            string="Image1",
            help="This field holds the image used as image for the category"),
        'image2': fields.binary(
            string="Image2",
            help="This field holds the image used as image for the category"),
        'image3': fields.binary(
            string="Image3",
            help="This field holds the image used as image for the category"),
        'image4': fields.binary(
            string="Image4",
            help="This field holds the image used as image for the category"),
        'profile_density': fields.float(string='Profile Density'),
        'comparison': fields.boolean(string='Comparison to competitors ?'),
        'square': fields.float(string='Area(in sq/m)'),
        'customer_ids': fields.many2many('res.partner',
                                         'res_partner_product_template_rel',
                                         'product_id',
                                         'partner_id',
                                         string='Customers'),
        'competitors_lines': fields.many2many(
            'competitors.products',
            'competitors_product_template_rel',
            'product_id',
            'competitor_product_id',
            string='Products'),
        'competitor_product': fields.many2one('competitors.products',
                                              string='Competitor Product'),
        'hasFoam': fields.boolean(string='Foam'),
        'article': fields.char(string='Article',
                               translate=True),
        'cost_matrics': fields.char(string='Cost Matrics',
                                    translate=True),
        'product_weight_category': fields.many2many(
            'product.weight.category',
            'product_template_product_weight_category_rel',
            'product_template_id',
            'product_weight_category_id',
            string='Product Categories'),
        'categ_width': fields.char(string='Category Width',
                                   translate=True),
        'custom_product_customer': fields.many2one('res.partner',
                                                   string='Customer'),
        'owned_by': fields.selection([
            ('undefined', ''),
            ('thermevo', 'THERMEVO(Own Products)'),
            ('gargiulo', 'Gargiulo'),
            ('Customer', 'Customer'), ], 'Owned By'),

        'attachment_pdf_id': fields.many2many(
            'ir.attachment',
            'decision_class_ir_attachments_pdf_rel',
            'pdf_class_id', 'attachment_id',
            string='PDF File'),
        'attachment_dxf_id': fields.many2many(
            'ir.attachment',
            'decision_class_ir_attachments_dxf_rel',
            'dxf_class_id', 'attachment_id',
            string='Attchment DXF'),
        'attachment_dwg_id': fields.many2many(
            'ir.attachment',
            'decision_class_ir_attachments_dwg_rel',
            'dwg_class_id', 'attachment_id',
            string='DWG File'),
        'attachment_pdf_adhesive_id': fields.many2many(
            'ir.attachment',
            'decision_class_ir_attachments_pdf_adhesive_rel',
            'pdf_class_id',
            'attachment_id',
            string='PDF File'),
        'attachment_dwg_adhesive_id': fields.many2many(
            'ir.attachment',
            'decision_class_ir_attachments_dwg_adhesive_rel',
            'dwg_class_id',
            'attachment_id',
            string='DWG File'),
        'attachment_pdf_foam_id': fields.many2many(
            'ir.attachment',
            'decision_class_ir_attachments_pdf_foam_rel',
            'pdf_class_id',
            'attachment_id',
            string='PDF File'),
        'attachment_dwg_foam_id': fields.many2many(
            'ir.attachment',
            'decision_class_ir_attachments_dwg_foam_rel',
            'dwg_class_id',
            'attachment_id', 'DWG File'),
        'attachment_pdf_foam_adhesive_id': fields.many2many(
            'ir.attachment',
            'decision_class_ir_attachments_pdf_foam_adhesive_rel',
            'pdf_class_id',
            'attachment_id',
            string='PDF File'),
        'attachment_dwg_foam_adhesive_id': fields.many2many(
            'ir.attachment',
            'decision_class_ir_attachments_dwg_foam_adhesive_rel',
            'dwg_class_id',
            'attachment_id',
            string='DWG File'),
        'question_ids': fields.many2many('forum.post',
                                         'res_forum_product_template_rel',
                                         'product_id',
                                         'forum_id',
                                         string='Questions'),
        'foam_product_id': fields.many2one('foam.product',
                                           string='Foam Product'),
        'res_id': fields.integer(string='Resource Id'),
        'lead_id': fields.integer(string='Lead Id'),
        'width': fields.char(string='Width',
                             translate=True),
        'length': fields.char(string='Length',
                              translate=True),
        'gluewire': fields.char(string='Gluewire',
                                translate=True),
        'foam': fields.char(string='Foam',
                            translate=True),
        'minimum_amount': fields.integer(string='Minimum Amount'),
        'product_material_ids': fields.many2many(
            'product.material',
            'product_material_template_rel',
            'material_id',
            'template_id',
            string='Product Material'),

        'product_optimal_qty_ids': fields.one2many(
            'product.optimal.qty',
            'product_id',
            string='Product Optimal QTY'),
        'minimum_template_price': fields.float(string='Minimum Template Price'),
        'stege_breite': fields.integer(string='Stream'),
        'qty_in_pallet': fields.integer(string='Bars In The Pallet'),

        'line_type_id': fields.many2one('production.line.type',
                                        string='Production Line Type'),
        'per_length': fields.boolean(string='Per Length'),
        'sell': fields.boolean(string='Sell')
    }
    _defaults = {
        'comparison': False,
        'per_length': True,
        'auto_variant_creation': False,
        'profile_density': 1.3,
        'owned_by': 'thermevo',
        'qty_in_pallet': 2400
    }

    @api.onchange('image_medium')
    def _onchange_image_medium(self):
        self.image4 = self.image_medium
        return

    @api.onchange('foam_product_id')
    def _onchange_foam_product_id(self):
        self.article = self.foam_product_id.article_number
        self.cost_matrics = self.foam_product_id.cost_metrics
        self.density = self.foam_product_id.foam_density
        return

    @api.onchange('square', 'profile_density')
    def _onchange_update_weight_net(self):
        self.weight_net = self.square * self.profile_density
        return

    @api.constrains('public_categ_ids')
    def _check_public_categ_ids(self):
        width_categories = []
        for public_category in self.public_categ_ids:
            if public_category.parent_id.name:
                if public_category.parent_id.name.upper() == 'WIDTH':
                    width_categories.append(public_category.parent_id.id)

        if len(width_categories) > 1:
            raise osv.except_osv(_("Error"),
                                 _("Can only have 1 width public category"))

    @api.onchange('public_categ_ids')
    def _onchange_public_categ_ids(self):
        _logger.debug(self)
        _logger.debug(self.public_categ_ids)
        for category in self.public_categ_ids:
            _logger.debug(category)
            if category.parent_id.name:
                _logger.debug(category.parent_id.name)
                if category.parent_id.name.upper() == 'WIDTH':
                    _logger.debug(category.parent_id.name.upper())
                    _logger.debug(category.name)
                    self.categ_width = category.name
                else:
                    self.categ_width = ''
        return

    @api.one
    @api.constrains('product_weight_category')
    def _check_foam_category(self):
        # attributes = [x.attribute_name for x in self.product_weight_category]
        attributes = []
        for x in self.product_weight_category:
            if x.attribute_name.single_category:
                attributes.append(x.attribute_name)

        if len(attributes) != len(set(attributes)):
            raise osv.except_osv(_("Error"), _(
                "Cannot have two categories of Foam attribute"))

    def create_article(self, cr, uid, ids, context=None):
        product_product = self.browse(cr, uid, ids, context=context)
        for product in product_product:
            material = '0'
            width = product.width
            try:
                width = float(width.replace(',', '.'))
            except:
                width = 6.0

            article_width = str(math.floor(width))
            geometric, new_article_square = search_geometric(product)
            if new_article_square == '0':
                article_square = str(round(math.sqrt(product.square), 2))
                article_square = article_square.split('.')
                new_article_square = '%s%s' % (
                    article_square[0], article_square[1])
                if len(article_square[1]) == 1:
                    new_article_square += '0'
            product_article = '%s%s%s%s.00000' % (
                str(material), geometric, article_width, new_article_square)
            default_code = product_article
            self.write(cr, uid, product.id,
                       {'article': product_article,
                        'default_code': default_code}, context=context)

    def write(self, cr, uid, ids, vals, context=None):
        if 'owned_by' in vals.keys():
            if vals['owned_by'] == 'Customer':
                vals['website_published'] = True

        if 'attachment_pdf_id' in vals:
            attachment_pdf_id_in_db = vals['attachment_pdf_id'][0][2]
            if len(attachment_pdf_id_in_db) > 1:
                raise osv.except_osv(_("Attachment Error"),
                                     _("You can not attach more than one PDF."))

            for attachments in vals['attachment_pdf_id'][0][2]:
                attachment_id1 = self.pool.get('ir.attachment')
                attachment_obj = attachment_id1.search(cr, uid, [
                    ('id', '=', attachments)])
                attachment_obj1 = attachment_id1.browse(cr, uid, attachment_obj)
                p = attachment_obj1.name.split('.')
                ext = p[len(p) - 1]
                if ext != 'pdf':
                    attachment_id1.unlink(cr, uid, attachment_obj1.id,
                                          context=context)
                    raise osv.except_osv(_("Error"), _("Accept only PDF file."))

        if 'attachment_pdf_adhesive_id' in vals:
            attachment_pdf_adhesive_id_in_db = \
                vals['attachment_pdf_adhesive_id'][0][2]
            if len(attachment_pdf_adhesive_id_in_db) > 1:
                raise osv.except_osv(_("Attachment Error"),
                                     _("You can not attach more than one PDF."))

            for attachments in vals['attachment_pdf_adhesive_id'][0][2]:
                attachment_id1 = self.pool.get('ir.attachment')
                attachment_obj = attachment_id1.search(cr, uid, [
                    ('id', '=', attachments)])
                attachment_obj1 = attachment_id1.browse(cr, uid, attachment_obj)
                p = attachment_obj1.name.split('.')
                ext = p[len(p) - 1]
                if ext != 'pdf':
                    attachment_id1.unlink(cr, uid, attachment_obj1.id,
                                          context=context)
                    raise osv.except_osv(_("Error"), _("Accept only PDF file."))

        if 'attachment_dwg_adhesive_id' in vals:
            attachment_dwg_adhesive_id_in_db = \
                vals['attachment_dwg_adhesive_id'][0][2]
            if len(attachment_dwg_adhesive_id_in_db) > 1:
                raise osv.except_osv(_("Attachment Error"),
                                     _("You can not attach more than one DWG."))

            for attachments in vals['attachment_dwg_adhesive_id'][0][2]:
                attachment_id1 = self.pool.get('ir.attachment')
                attachment_obj = attachment_id1.search(cr, uid, [
                    ('id', '=', attachments)])
                attachment_obj1 = attachment_id1.browse(cr, uid, attachment_obj)
                p = attachment_obj1.name.split('.')
                ext = p[len(p) - 1]
                if ext != 'dwg':
                    attachment_id1.unlink(cr, uid, attachment_obj1.id,
                                          context=context)
                    raise osv.except_osv(_("Error"), _("Accept only DWG file."))

        if 'attachment_pdf_foam_id' in vals:
            attachment_pdf_foam_id_in_db = vals['attachment_pdf_foam_id'][0][2]
            if len(attachment_pdf_foam_id_in_db) > 1:
                raise osv.except_osv(_("Attachment Error"),
                                     _("You can not attach more than one PDF."))

            for attachments in vals['attachment_pdf_foam_id'][0][2]:
                attachment_id1 = self.pool.get('ir.attachment')
                attachment_obj = attachment_id1.search(cr, uid, [
                    ('id', '=', attachments)])
                attachment_obj1 = attachment_id1.browse(cr, uid, attachment_obj)
                p = attachment_obj1.name.split('.')
                ext = p[len(p) - 1]
                if ext != 'pdf':
                    attachment_id1.unlink(cr, uid, attachment_obj1.id,
                                          context=context)
                    raise osv.except_osv(_("Error"), _("Accept only PDF file."))

        if 'attachment_dwg_foam_id' in vals:
            attachment_dwg_foam_id_in_db = vals['attachment_dwg_foam_id'][0][2]
            if len(attachment_dwg_foam_id_in_db) > 1:
                raise osv.except_osv(_("Attachment Error"),
                                     _("You can not attach more than one DWG."))

            for attachments in vals['attachment_dwg_foam_id'][0][2]:
                attachment_id1 = self.pool.get('ir.attachment')
                attachment_obj = attachment_id1.search(cr, uid, [
                    ('id', '=', attachments)])
                attachment_obj1 = attachment_id1.browse(cr, uid, attachment_obj)
                p = attachment_obj1.name.split('.')
                ext = p[len(p) - 1]
                if ext != 'dwg':
                    attachment_id1.unlink(cr, uid, attachment_obj1.id,
                                          context=context)
                    raise osv.except_osv(_("Error"), _("Accept only DWG file."))

        if 'attachment_pdf_foam_adhesive_id' in vals:
            attachment_pdf_foam_adhesive_id_in_db = \
                vals['attachment_pdf_foam_adhesive_id'][0][2]
            if len(attachment_pdf_foam_adhesive_id_in_db) > 1:
                raise osv.except_osv(_("Attachment Error"),
                                     _("You can not attach more than one PDF."))

            for attachments in vals['attachment_pdf_foam_adhesive_id'][0][2]:
                attachment_id1 = self.pool.get('ir.attachment')
                attachment_obj = attachment_id1.search(cr, uid, [
                    ('id', '=', attachments)])
                attachment_obj1 = attachment_id1.browse(cr, uid, attachment_obj)
                p = attachment_obj1.name.split('.')
                ext = p[len(p) - 1]
                if ext != 'pdf':
                    attachment_id1.unlink(cr, uid, attachment_obj1.id,
                                          context=context)
                    raise osv.except_osv(_("Error"), _("Accept only PDF file."))

        if 'attachment_dwg_foam_adhesive_id' in vals:
            attachment_dwg_foam_adhesive_id_in_db = \
                vals['attachment_dwg_foam_adhesive_id'][0][2]
            if len(attachment_dwg_foam_adhesive_id_in_db) > 1:
                raise osv.except_osv(_("Attachment Error"),
                                     _("You can not attach more than one DWG."))

            for attachments in vals['attachment_dwg_foam_adhesive_id'][0][2]:
                attachment_id1 = self.pool.get('ir.attachment')
                attachment_obj = attachment_id1.search(cr, uid, [
                    ('id', '=', attachments)])
                attachment_obj1 = attachment_id1.browse(cr, uid, attachment_obj)
                p = attachment_obj1.name.split('.')
                ext = p[len(p) - 1]
                if ext != 'dwg':
                    attachment_id1.unlink(cr, uid, attachment_obj1.id,
                                          context=context)
                    raise osv.except_osv(_("Error"), _("Accept only DWG file."))

        if 'attachment_dxf_id' in vals:
            attachment_dxf_id_in_db = vals['attachment_dxf_id'][0][2]
            if len(attachment_dxf_id_in_db) > 1:
                raise osv.except_osv(_("Attachment Error"),
                                     _("You can not attach more than one DXF."))

            for attachments1 in vals['attachment_dxf_id'][0][2]:
                attachment_id2 = self.pool.get('ir.attachment')
                attachment_obj12 = attachment_id2.search(cr, uid, [
                    ('id', '=', attachments1)])
                attachment_obj13 = attachment_id2.browse(cr, uid,
                                                         attachment_obj12)
                p1 = attachment_obj13.name.split('.')
                ext = p1[len(p1) - 1]
                if ext != 'dxf':
                    attachment_id2.unlink(cr, uid, attachment_obj13.id,
                                          context=context)
                    raise osv.except_osv(_("Error"), _("Accept only DXF file."))

        if 'attachment_dwg_id' in vals:
            attachment_dwg_id_in_db = vals['attachment_dwg_id'][0][2]
            if len(attachment_dwg_id_in_db) > 1:
                raise osv.except_osv(_("Attachment Error"),
                                     _("You can not attach more than one DWG."))

            for attachments2 in vals['attachment_dwg_id'][0][2]:
                attachment_id3 = self.pool.get('ir.attachment')
                attachment_obj123 = attachment_id3.search(cr, uid, [
                    ('id', '=', attachments2)])
                attachment_obj133 = attachment_id3.browse(cr, uid,
                                                          attachment_obj123)
                p1 = attachment_obj133.name.split('.')
                ext = p1[len(p1) - 1]
                if ext != 'dwg':
                    attachment_id3.unlink(cr, uid, attachment_obj133.id,
                                          context=context)
                    raise osv.except_osv(_("Error"), _("Accept only DWG file."))

        result = super(ProductTemplate, self).write(cr, uid, ids, vals,
                                                    context=context)
        return result

    def create(self, cr, uid, vals, context=None):
        default_product_config_pool = self.pool.get('default.product.config')
        default_product_config_id = default_product_config_pool.search(
            cr, uid, [], limit=1)
        default_product_config = default_product_config_pool.browse(
            cr, uid, default_product_config_id, context=context)
        try:
            default_product_config = default_product_config[0]
        except:
            pass

        if default_product_config:
            vals.update(
                property_account_income=
                default_product_config.property_account_income.id,
                taxes_id=[
                    (6, 0, [e.id for e in default_product_config.taxes_ids])],
                property_account_expense=
                default_product_config.property_account_expense.id,
                supplier_taxes_id=[
                    (6, 0,
                     [e.id for e in
                      default_product_config.supplier_taxes_ids])])
        if 'res_id' in vals:
            product_id = super(ProductTemplate, self).create(
                cr, uid, vals, context=context)
            return product_id

        if context is None:
            context = {}
        create_context = dict(context)
        if 'owned_by' in vals.keys():
            if vals['owned_by'] == 'Customer':
                vals['website_published'] = True

        if 'attachment_pdf_id' in vals:
            attachment_pdf_id_in_db = vals['attachment_pdf_id'][0][2]
            if len(attachment_pdf_id_in_db) > 1:
                raise osv.except_osv(_("Attachment Error"),
                                     _("You can not attach more than one PDF."))

            for attachments in vals['attachment_pdf_id'][0][2]:
                attachment_obj = self.pool.get('ir.attachment').search(
                    cr, uid, [('id', '=', attachments)])
                attachment_obj1 = self.pool.get('ir.attachment').browse(
                    cr, uid, attachment_obj)
                p = attachment_obj1.name.split('.')
                ext = p[len(p) - 1]
                if ext != 'pdf':
                    self.pool.get('ir.attachment').unlink(
                        cr, uid, attachment_obj1.id, context=context)
                    raise osv.except_osv(_("Error"), _("Accept only PDF file."))

        if 'attachment_pdf_adhesive_id' in vals:
            attachment_pdf_adhesive_id_in_db = \
                vals['attachment_pdf_adhesive_id'][0][2]
            if len(attachment_pdf_adhesive_id_in_db) > 1:
                raise osv.except_osv(_("Attachment Error"),
                                     _("You can not attach more than one PDF."))

            for attachments in vals['attachment_pdf_adhesive_id'][0][2]:
                attachment_obj = self.pool.get('ir.attachment').search(
                    cr, uid, [('id', '=', attachments)])
                attachment_obj1 = self.pool.get('ir.attachment').browse(
                    cr, uid, attachment_obj)
                p = attachment_obj1.name.split('.')
                ext = p[len(p) - 1]
                if ext != 'pdf':
                    self.pool.get('ir.attachment').unlink(cr, uid,
                                                          attachment_obj1.id,
                                                          context=context)
                    raise osv.except_osv(_("Error"), _("Accept only PDF file."))

        if 'attachment_dwg_adhesive_id' in vals:
            attachment_dwg_adhesive_id_in_db = \
                vals['attachment_dwg_adhesive_id'][0][2]
            if len(attachment_dwg_adhesive_id_in_db) > 1:
                raise osv.except_osv(_("Attachment Error"),
                                     _("You can not attach more than one DWG."))

            for attachments in vals['attachment_dwg_adhesive_id'][0][2]:
                attachment_obj = self.pool.get('ir.attachment').search(
                    cr, uid, [('id', '=', attachments)])
                attachment_obj1 = self.pool.get('ir.attachment').browse(
                    cr, uid, attachment_obj)
                p = attachment_obj1.name.split('.')
                ext = p[len(p) - 1]
                if ext != 'dwg':
                    self.pool.get('ir.attachment').unlink(cr, uid,
                                                          attachment_obj1.id,
                                                          context=context)
                    raise osv.except_osv(_("Error"), _("Accept only DWG file."))

        if 'attachment_pdf_foam_id' in vals:
            attachment_pdf_foam_id_in_db = vals['attachment_pdf_foam_id'][0][2]
            if len(attachment_pdf_foam_id_in_db) > 1:
                raise osv.except_osv(_("Attachment Error"),
                                     _("You can not attach more than one PDF."))

            for attachments in vals['attachment_pdf_foam_id'][0][2]:
                attachment_obj = self.pool.get('ir.attachment').search(
                    cr, uid, [('id', '=', attachments)])
                attachment_obj1 = self.pool.get('ir.attachment').browse(
                    cr, uid, attachment_obj)
                p = attachment_obj1.name.split('.')
                ext = p[len(p) - 1]
                if ext != 'pdf':
                    self.pool.get('ir.attachment').unlink(cr, uid,
                                                          attachment_obj1.id,
                                                          context=context)
                    raise osv.except_osv(_("Error"), _("Accept only PDF file."))

        if 'attachment_dwg_foam_id' in vals:
            attachment_dwg_foam_id_in_db = vals['attachment_dwg_foam_id'][0][2]
            if len(attachment_dwg_foam_id_in_db) > 1:
                raise osv.except_osv(_("Attachment Error"),
                                     _("You can not attach more than one DWG."))

            for attachments in vals['attachment_dwg_foam_id'][0][2]:
                attachment_obj = self.pool.get('ir.attachment').search(
                    cr, uid, [('id', '=', attachments)])
                attachment_obj1 = self.pool.get('ir.attachment').browse(
                    cr, uid, attachment_obj)
                p = attachment_obj1.name.split('.')
                ext = p[len(p) - 1]
                if ext != 'dwg':
                    self.pool.get('ir.attachment').unlink(cr, uid,
                                                          attachment_obj1.id,
                                                          context=context)
                    raise osv.except_osv(_("Error"), _("Accept only DWG file."))

        if 'attachment_pdf_foam_adhesive_id' in vals:
            attachment_pdf_foam_adhesive_id_in_db = \
                vals['attachment_pdf_foam_adhesive_id'][0][2]
            if len(attachment_pdf_foam_adhesive_id_in_db) > 1:
                raise osv.except_osv(_("Attachment Error"),
                                     _("You can not attach more than one PDF."))

            for attachments in vals['attachment_pdf_foam_adhesive_id'][0][2]:
                attachment_obj = self.pool.get('ir.attachment').search(
                    cr, uid, [('id', '=', attachments)])
                attachment_obj1 = self.pool.get('ir.attachment').browse(
                    cr, uid, attachment_obj)
                p = attachment_obj1.name.split('.')
                ext = p[len(p) - 1]
                if ext != 'pdf':
                    self.pool.get('ir.attachment').unlink(cr, uid,
                                                          attachment_obj1.id,
                                                          context=context)
                    raise osv.except_osv(_("Error"), _("Accept only PDF file."))

        if 'attachment_dwg_foam_adhesive_id' in vals:
            attachment_dwg_foam_adhesive_id_in_db = \
                vals['attachment_dwg_foam_adhesive_id'][0][2]
            if len(attachment_dwg_foam_adhesive_id_in_db) > 1:
                raise osv.except_osv(_("Attachment Error"),
                                     _("You can not attach more than one DWG."))

            for attachments in vals['attachment_dwg_foam_adhesive_id'][0][2]:
                attachment_obj = self.pool.get('ir.attachment').search(
                    cr, uid, [('id', '=', attachments)])
                attachment_obj1 = self.pool.get('ir.attachment').browse(
                    cr, uid, attachment_obj)
                p = attachment_obj1.name.split('.')
                ext = p[len(p) - 1]
                if ext != 'dwg':
                    self.pool.get('ir.attachment').unlink(cr, uid,
                                                          attachment_obj1.id,
                                                          context=context)
                    raise osv.except_osv(_("Error"), _("Accept only DWG file."))

        if 'attachment_dxf_id' in vals:
            attachment_dxf_id_in_db = vals['attachment_dxf_id'][0][2]
            if len(attachment_dxf_id_in_db) > 1:
                raise osv.except_osv(_("Attachment Error"),
                                     _("You can not attach more than one DXF."))

            for attachments1 in vals['attachment_dxf_id'][0][2]:
                attachment_obj2 = self.pool.get('ir.attachment').search(
                    cr, uid, [('id', '=', attachments1)])
                attachment_obj3 = self.pool.get('ir.attachment').browse(
                    cr, uid, attachment_obj2)
                p1 = attachment_obj3.name.split('.')
                ext = p1[len(p1) - 1]
                if ext != 'dxf':
                    self.pool.get('ir.attachment').unlink(cr, uid,
                                                          attachment_obj3.id,
                                                          context=context)
                    raise osv.except_osv(_("Error"), _("Accept only DXF file."))

        if 'attachment_dwg_id' in vals:
            attachment_dwg_id_in_db = vals['attachment_dwg_id'][0][2]
            if len(attachment_dwg_id_in_db) > 1:
                raise osv.except_osv(_("Attachment Error"),
                                     _("You can not attach more than one DWG."))

            for attachments2 in vals['attachment_dwg_id'][0][2]:
                attachment_obj21 = self.pool.get('ir.attachment').search(
                    cr, uid, [('id', '=', attachments2)])
                attachment_obj31 = self.pool.get('ir.attachment').browse(
                    cr, uid, attachment_obj21)
                p1 = attachment_obj31.name.split('.')
                ext = p1[len(p1) - 1]
                if ext != 'dwg':
                    self.pool.get('ir.attachment').unlink(cr, uid,
                                                          attachment_obj31.id,
                                                          context=context)
                    raise osv.except_osv(_("Error"), _("Accept only DWG file."))

        product_id = super(ProductTemplate, self).create(cr, uid, vals,
                                                         context=create_context)

        product_template_obj = self.pool.get('product.template')

        field_name = 'image1'
        product_read = \
            product_template_obj.read(cr, uid, [product_id], [field_name],
                                      context=context)[0]
        super(ProductTemplate, self).write(cr, uid, [product_id], {
            'image_medium': product_read['image1']}, context=context)
        # current_user = self.pool.get('res.users').browse(cr, uid, uid)

        return product_id


class ProductAttribute(osv.osv):
    _inherit = 'product.attribute'
    _sql_constraints = [
        ('attribute_name_uniq', 'unique (name)',
         'This attribute already exists !')
    ]


class WeightRange(osv.osv):
    _name = 'weight.range'

    _columns = {
        'name': fields.char('Name', translate=True, size=25),
        'min_weight': fields.float('Minimum'),
        'max_weight': fields.float('Maximum'),
    }
    _defaults = {
        'min_weight': 0.00,
        'max_weight': 0.00
    }

    def _check_min_max(self, cr, uid, ids, context=None):
        obj = self.browse(cr, uid, ids[0], context=context)
        if obj.min_weight > obj.max_weight:
            return False
        return True

    _constraints = [
        (_check_min_max, 'Maximum Value should be more than Minimum Value',
         ['min_weight', 'max_weight']),
    ]

    @api.onchange('min_weight', 'max_weight')
    def _onchange_min_max_weight(self):
        self.name = str(self.min_weight) + " - " + str(self.max_weight)
        return

    def create(self, cr, uid, vals, context=None):
        if not vals["min_weight"]:
            vals["min_weight"] = 0.00
        if not vals["max_weight"]:
            vals["max_weight"] = 0.00
        vals["name"] = str(vals["min_weight"]) + " - " + str(vals["max_weight"])
        result = super(WeightRange, self).create(cr, uid, vals, context=context)
        return result

    def write(self, cr, uid, ids, vals, context=None):
        new_dict = {}
        old_ids = self.browse(cr, uid, ids, context=context)
        try:
            new_dict["name"] = vals["name"]
        except:
            new_dict["name"] = old_ids[0].name
        try:
            new_dict["min_weight"] = vals["min_weight"]
        except:
            new_dict["min_weight"] = old_ids[0].min_weight
        try:
            new_dict["max_weight"] = vals["max_weight"]
        except:
            new_dict["max_weight"] = old_ids[0].max_weight
        if not new_dict["min_weight"]:
            new_dict["min_weight"] = 0.00
        if not new_dict["max_weight"]:
            new_dict["max_weight"] = 0.00

        new_dict["name"] = (str(new_dict["min_weight"]) + " - " +
                            str(new_dict["max_weight"]))

        result = super(WeightRange, self).write(cr, uid, ids, new_dict,
                                                context=context)
        return result


class FoamDimensionsLimit(osv.osv):
    _name = 'foam.dimensions.limit'

    _columns = {
        'name': fields.char('Name',
                            translate=True),
        'min_width': fields.float('Minimum Width'),
        'min_height': fields.float('Minimum Height'),
        'fixed_width': fields.selection(
            (('25', 25),
             ('31', 31),
             ('39', 39)),
            'Width'),
        'max_width': fields.float('Maximum Width'),
        'max_height': fields.float('Maximum Height'),
        'fixed_profile_width_ids': fields.many2many(
            'fixed.width.profile',
            'res_foam_dimention_width_rel',
            'dimention_width_id',
            'foam_dimention_width_id',
            'Profile Width Step',
            required=True),
        'fixed_profile_height_ids': fields.many2many(
            'fixed.height.profile',
            'res_foam_dimention_height_rel',
            'dimention_height_id',
            'foam_dimention_height_id',
            'Profile Height Step',
            required=True),
    }


class FixedWidthProfile(osv.osv):
    _name = 'fixed.width.profile'

    _columns = {
        'name': fields.char('Name',
                            translate=True,
                            invisible=True),
        'product_fixed_profile_width_id': fields.many2one('product.template',
                                                          'Foam Dimention',
                                                          invisible=True),
        'fixed_profile_width_id': fields.many2one('foam.dimensions.limit',
                                                  'Foam Dimention',
                                                  invisible=True),
        'number': fields.integer('Number')

    }

    @api.onchange('name')
    def _onchange_profile_width_name(self):
        self.name = str(self.number)
        return

    def create(self, cr, uid, vals, context=None):
        vals["name"] = str(vals["number"])
        result = super(FixedWidthProfile, self).create(
            cr, uid, vals, context=context)
        return result


class FixedHeightProfile(osv.osv):
    _name = 'fixed.height.profile'

    _columns = {
        'name': fields.char('Name',
                            translate=True),
        'product_fixed_profile_heitht_id': fields.many2one('product.template',
                                                           'Foam Dimention',
                                                           invisible=True),
        'fixed_profile_heitht_id': fields.many2one('foam.dimensions.limit',
                                                   'Foam Dimention',
                                                   invisible=True),
        'number': fields.integer('Number')

    }

    @api.onchange('name')
    def _onchange_profile_width_name(self):
        self.name = str(self.number)
        return

    def create(self, cr, uid, vals, context=None):
        vals["name"] = str(vals["number"])
        result = super(FixedHeightProfile, self).create(
            cr, uid, vals, context=context)
        return result


class ProductWeightCategory(osv.osv):
    _name = 'product.weight.category'

    _columns = {
        'name': fields.char('Name',
                            translate=True,
                            size=25),
        'attribute_name': fields.many2one('attribute.range',
                                          'Attribute',
                                          required=True),
        'range': fields.many2one('weight.range', 'Range'),
        'price': fields.float('Price'),
        'foam_dimensions_limit': fields.many2one('foam.dimensions.limit',
                                                 'Foam Dimensions'),
        'temp1': fields.text('OnChange',
                             invisible=1),
        'ems': fields.float('EMS (€/m)'),
        'attribute_range_related': fields.related('attribute_name',
                                                  'is_eligible_ems',
                                                  string="Related",
                                                  relation="attribute.range",
                                                  type="boolean")
    }

    @api.onchange('attribute_name')
    def _onchange_attribute_name(self):
        if self.attribute_name.foam and not (
                self.attribute_name.single_category):
            self.temp1 = 'foam'
        else:
            self.temp1 = 'no_foam'
        return


class AttributeRange(osv.osv):
    _name = 'attribute.range'
    _columns = {
        'name': fields.char('Attribute',
                            translate=True,
                            size=25),
        'single_category': fields.boolean('Single Category'),
        'foam': fields.boolean('Foam'),
        'is_eligible_ems': fields.boolean('EMS ?'),
    }


class ProductAttributeLine(osv.osv):
    _inherit = 'product.attribute.line'

    def create(self, cr, uid, vals, context=None):
        if not context.get('insulating_product'):
            product_attribute_price = self.pool.get('product.attribute.price')
            create_value_list = vals["value_ids"][0][2]
            values = {}
            for i in create_value_list:
                values["value_id"] = i
                values["product_tmpl_id"] = vals["product_tmpl_id"]
                values["price_extra"] = 0.0
                check_exist = product_attribute_price.search(
                    cr, uid, [('value_id', '=', i)], context=context)
                if check_exist:
                    got_instane = product_attribute_price.browse(
                        cr, uid, [check_exist[0]], context=context)
                    values["price_extra"] = got_instane.price_extra
                product_attribute_price.create(cr, uid, values, context=context)
        new_product_attribute_line_id = super(
            ProductAttributeLine, self).create(cr, uid, vals, context=context)
        return new_product_attribute_line_id

    def write(self, cr, uid, ids, vals, context=None):
        create_value_list = vals["value_ids"][0][2]
        vals["value_ids"][0][2] = list(set(create_value_list))
        already_id = self.browse(cr, uid, ids, context=context)
        product_attribute_price = self.pool.get('product.attribute.price')
        for i in create_value_list:
            check_exist = product_attribute_price.search(
                cr, uid, [
                    ('value_id', '=', i),
                    ('product_tmpl_id', '=', already_id.product_tmpl_id.id)],
                context=context)
            if not check_exist:
                values = {}
                values["value_id"] = i
                values["product_tmpl_id"] = already_id.product_tmpl_id.id
                values["price_extra"] = 0.00
                product_attribute_price.create(cr, uid, values, context=context)
        result = super(ProductAttributeLine, self).write(cr, uid, ids, vals,
                                                         context=context)
        return result


class ProductAttributePrice(osv.osv):
    _inherit = 'product.attribute.price'

    def create(self, cr, uid, vals, context=None):
        new_price_id = super(ProductAttributePrice, self).create(
            cr, uid, vals, context=context);
        product_attribute_price_ids = self.search(
            cr, uid,
            [('value_id', '=', vals["value_id"])],
            context=context)
        product_attribute_price_ids.append(new_price_id)
        # print "\n\n\n product_attribute_price_ids in create ==> 
        # ",product_attribute_price_ids
        for i in product_attribute_price_ids:
            self.write(cr, uid, i, {"price_extra": vals["price_extra"]},
                       context=context)
        pav = self.pool.get('product.attribute.value').browse(cr, uid, [
            int(vals["value_id"])], context=context)
        return new_price_id

    def write(self, cr, uid, ids, vals, context=None):
        current_price = self.browse(cr, uid, ids, context=context)
        product_attribute_price_ids = self.search(cr, uid, [
            ('value_id', '=', current_price.value_id.id)], context=context)
        # print "\n\n\n Product Attribute Price for Write ==> 
        # ",product_attribute_price_ids
        if vals.has_key('apply'):
            result = super(ProductAttributePrice, self).write(
                cr, uid, ids, vals, context=context)
            test_data = self.browse(cr, uid, ids, context=context)
        else:
            result = super(ProductAttributePrice, self).write(
                cr, uid, product_attribute_price_ids, vals, context=context)
        return result


class ProductPublicCategory(osv.osv):
    _inherit = 'product.public.category'
    _columns = {
        'main_category': fields.boolean('Product Main Category'),
        'default': fields.boolean("Default Category"),
        'width': fields.boolean("Display as Width"),
        'geometrie': fields.boolean("Display as Geometrie"),
    }

    @api.constrains('name')
    def _check_duplicate_name(self):
        if len(self.search([['name', '=', self.name]])) > 1:
            raise osv.except_osv(_("Error"), _("Entered Name Already Exists"))


class FoamProduct(osv.osv):
    _name = 'foam.product'
    _description = 'Foam Product'
    _columns = {
        'name': fields.char('Name',
                            translate=True,
                            size=25),
        'minimum_width': fields.integer('Minimum Width'),
        'maximum_width': fields.integer('Maximum Width'),
        'width_step': fields.integer('Width Step'),
        'mimimum_height': fields.integer('Minimum Height'),
        'maximum_height': fields.integer('Maximum Height'),
        'height_step': fields.integer('Height Step'),
        'fixed_foam_width_ids': fields.one2many('fixed.width.foam',
                                                'fixed_foam_width_id',
                                                'Width Step'),
        'fixed_foam_height_ids': fields.one2many('fixed.height.foam',
                                                 'fixed_foam_height_id',
                                                 'Height Step'),
        'foam_density': fields.float('Foam Density'),
        'foam_min_length': fields.integer('Foam Minimum Length'),
        'foam_max_length': fields.integer('Foam Maximum Length'),
        'cost_metrics': fields.char('Cost Metrics',
                                    translate=True,
                                    size=25),
        'article_number': fields.char('Article Number',
                                      translate=True,
                                      size=25),
    }


class FixedWidthFoam(osv.osv):
    _name = 'fixed.width.foam'

    _columns = {
        'name': fields.char('Name',
                            translate=True,
                            invisible=True),
        'fixed_foam_width_id': fields.many2one('foam.product',
                                               'Foam Product',
                                               invisible=True),
        'number': fields.integer('Number')

    }

    @api.onchange('name')
    def _onchange_profile_width_name(self):
        self.name = str(self.number)
        return

    def create(self, cr, uid, vals, context=None):
        vals["name"] = str(vals["number"])
        result = super(FixedWidthFoam, self).create(
            cr, uid, vals, context=context)
        return result


class FixedHeightFoam(osv.osv):
    _name = 'fixed.height.foam'

    _columns = {
        'name': fields.char('Name',
                            translate=True),
        'fixed_foam_height_id': fields.many2one('foam.product',
                                                'Foam Product',
                                                invisible=True),
        'number': fields.integer('Number')

    }

    @api.onchange('name')
    def _onchange_profile_width_name(self):
        self.name = str(self.number)
        return

    def create(self, cr, uid, vals, context=None):
        vals["name"] = str(vals["number"])
        result = super(FixedHeightFoam, self).create(
            cr, uid, vals, context=context)
        return result


class DefaultProductConfig(osv.osv):
    _name = "default.product.config"

    _columns = {
        'property_account_income': fields.many2one(
            'account.account',
            string='Property Account Incoming',
            required=True),
        'property_account_expense': fields.many2one(
            'account.account',
            string='Property Account Expense',
            required=True),

        'taxes_ids': fields.many2many(
            'account.tax',
            'default_product_taxes_rel',
            'prod_id', 'tax_id', 'Customer Taxes',
            domain=[('parent_id', '=', False),
                    ('type_tax_use', 'in', ['sale', 'all'])]),
        'supplier_taxes_ids': fields.many2many(
            'account.tax',
            'default_product_supplier_taxes_rel',
            'prod_id',
            'tax_id',
            'Supplier Taxes',
            domain=[('parent_id', '=', False),
                    ('type_tax_use', 'in', ['purchase', 'all'])]),
    }
