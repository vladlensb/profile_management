# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Business Applications
#    Copyright (c) 2012 OpenERP S.A. <http://openerp.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from openerp.osv import osv, fields
from openerp.tools.translate import _
import logging
_logger = logging.getLogger(__name__)

class ResPartner(osv.osv):
    _inherit = 'res.partner'

    def search_read(self, cr, uid, domain=None, fields=None, offset=0,
                    limit=None, order=None, context=None):
        crm_case_section_pool = self.pool.get('crm.case.section')
        crm_case_section_ids = crm_case_section_pool.search(
            cr, uid, [], context=context)
        crm_case_sections = crm_case_section_pool.browse(
            cr, uid, crm_case_section_ids, context=context)

        res_users_pool = self.pool.get('res.users')
        res_user = res_users_pool.browse(cr, uid, uid, context=context)

        section_ids = []
        for crm_case_section in crm_case_sections:
            if res_user in crm_case_section.member_ids:
                section_ids.append(crm_case_section.id)
        result = []
        if section_ids:
            cr.execute("""
                            SELECT id
                            FROM res_partner
                            WHERE section_id IN %s
                            """, (tuple(section_ids),))

            result = [x[0] for x in cr.fetchall()]
        partner_ids = []
        false_partner_ids = []
        for partner_id in self.search(cr, uid, [], context=context):
            if partner_id in result:
                partner_ids.append(partner_id)
            else:
                false_partner_ids.append(partner_id)
        self.write(
            cr, uid, partner_ids, {'ch_team': 'true'}, context=context)
        self.write(
            cr, uid, false_partner_ids, {'ch_team': 'false'},
            context=context)
        return super(ResPartner, self).search_read(
            cr, uid, domain=domain, fields=fields, offset=offset, limit=limit,
            order=order, context=context)

    _columns = {
        'mails_data': fields.one2many('mail.mail.statistics',
                                      'partner_name',
                                      string='Mails'),
        'position': fields.char(string='Position',
                                translate=True,
                                size=100),
        'title': fields.char(string='Title',
                             translate=True,
                             size=100),
        'job_title': fields.char(string='Job Title',
                                 translate=True,
                                 size=100),
        'is_current': fields.char(string='Is Current?',
                                  translate=True,
                                  size=100),
        'location': fields.char(string='Location',
                                translate=True,
                                size=100),
        'company_name': fields.char(string='Company Name',
                                    translate=True,
                                    size=100),
        'country_code': fields.char(string='Country Code',
                                    translate=True,
                                    size=100),
        'current_share_title': fields.char(string='Current Share Title',
                                           translate=True,
                                           size=100),
        'current_share_description': fields.text(
            string='Current Share Description',
            translate=True),
        'numConnections': fields.char(string='Number of Connections',
                                      translate=True,
                                      size=100),
        'company_type': fields.char(string='Company Type',
                                    translate=True,
                                    size=100),
        'company_ticker': fields.char(string='Company Ticker',
                                      translate=True,
                                      size=100),
        'industry': fields.char(string='Industry',
                                translate=True,
                                size=100),
        'c_id': fields.char(string='Company ID',
                            translate=True,
                            size=100),
        'last_name': fields.char(string='Last Name',
                                 translate=True),
        'annual_volume': fields.integer(string='Annual Purchase Volume'),
        'signature': fields.char(string='Signature'),
        'ch_team': fields.boolean(srting='ch_team',
                                  default=True),
        'e_low_open_groove': fields.boolean('Allow Open Groove Gluewire'),
    }

    def button_check_vat(self, cr, uid, ids, context=None):
        if not self.check_vat(cr, uid, ids, context=context):
            msg = self._construct_constraint_msg(cr, uid, ids, context=context)
            raise osv.except_osv(_('Error!'), msg)
        vat = self.browse(cr, uid, ids, context=context).vat
        if not vat:
            raise osv.except_osv(_('Error!'), _('You have no VAT number'))
        country_code = vat[0:2]
        res_country_pool = self.pool.get('res.country')
        country_group_pool = self.pool.get('res.country.group')
        res_country_ids = res_country_pool.search(
            cr, uid, [('code', '=', country_code)], context=context)
        res_country_objects = res_country_pool.browse(
            cr, uid, res_country_ids, context=context)
        country_group_ids = country_group_pool.search(
            cr, uid, [], context=context)
        country_group_objects = country_group_pool.browse(
            cr, uid, country_group_ids, context=context)
        for country_group_object in country_group_objects:
            _logger.info(country_group_object.country_ids)
            if country_group_object.country_ids and \
                            res_country_objects \
                            in country_group_object.country_ids:
                _logger.info('\n')
                _logger.info(country_group_object.account_fiscal_position_id.name)
                _logger.info('\n')
                self.write(cr, uid, ids, {
                    'property_account_position':
                        country_group_object.account_fiscal_position_id.id},
                           context=context)
        return True

    def create(self, cr, uid, values, context=None):
        country_group_pool = self.pool.get('res.country.group')
        if values.get('country_id'):
            res_country_pool = self.pool.get('res.country')
            res_country_object = res_country_pool.browse(
                cr, uid, values.get('country_id'), context=context)
            if values.get('vat'):
                country_code = values.get('vat')[0:2]
                res_country_id = res_country_pool.search(
                    cr, uid, [('code', '=', country_code)], context=context)
                if values.get('country_id') != res_country_id:
                    country_group_ids = country_group_pool.search(
                        cr, uid, [
                            ('default_country_group', '=', True)],
                        context=context)
                    country_group_objects = country_group_pool.browse(
                        cr, uid, country_group_ids, context=context)
                    values[
                        'property_account_position'] = country_group_objects.default_account_fiscal_position_id
                    return super(ResPartner, self).create(
                        cr, uid, values, context=context)
            country_group_ids = country_group_pool.search(
                cr, uid, [], context=context)
            country_group_objects = country_group_pool.browse(
                cr, uid, country_group_ids, context=context)
            for country_group_object in country_group_objects:
                if res_country_object in country_group_object.country_ids:
                    values[
                        'property_account_position'] = country_group_object.default_account_fiscal_position_id
        else:
            country_group_ids = country_group_pool.search(
                cr, uid, [('default_country_group', '=', True)],
                context=context)
            country_group_objects = country_group_pool.browse(
                cr, uid, country_group_ids, context=context)
            values[
                'property_account_position'] = country_group_objects.default_account_fiscal_position_id
        return super(ResPartner, self).create(cr, uid, values, context=context)

    # def write(self, cr, uid, ids, values, context=None):
    #     if values.get('country_id'):
    #         country_group_pool = self.pool.get('res.country.group')
    #         res_country_pool = self.pool.get('res.country')
    #         res_country_object = res_country_pool.browse(
    #             cr, uid, int(values.get('country_id')), context=context)
    #         country_group_ids = country_group_pool.search(
    #             cr, uid, [], context=context)
    #         country_group_objects = country_group_pool.browse(
    #             cr, uid, country_group_ids, context=context)
    #         for country_group_object in country_group_objects:
    #             if res_country_object in country_group_object.country_ids:
    #                 values['property_account_position'] = \
    #                     country_group_object.default_account_fiscal_position_id
    #     return super(ResPartner, self).write(
    #         cr, uid, ids, values, context=context)


ResPartner()


class res_partner_bank(osv.osv):
    _inherit = 'res.partner.bank'

    _columns = {
        'vat_number': fields.char(
            'TIN',
            help="Tax Identification Number. Check the box if this contact "
                 "is subjected to taxes. "
                 "Used by the some of the legal statements."),
    }


res_partner_bank()


class res_company(osv.osv):
    _inherit = 'res.company'


class hr_employee(osv.osv):
    _inherit = 'hr.employee'


class CountryGroup(osv.osv):
    _name = 'res.country.group'
    _inherit = 'res.country.group'

    _columns = {
        'account_fiscal_position_id': fields.many2one(
            'account.fiscal.position',
            srting='Fiscal Position with VAT'),
        'default_account_fiscal_position_id': fields.many2one(
            'account.fiscal.position',
            srting='Default Fiscal Position'),
        'default_country_group': fields.boolean(srting='Default Group'),
    }

    def write(self, cr, uid, ids, vals, context=None):
        if vals.get('default_country_group'):
            group_ids = self.search(
                cr, uid, [('id', '!=', ids)], context=context)
            self.write(cr, uid, group_ids, {'default_country_group': False},
                       context=context)
        return super(CountryGroup, self).write(
            cr, uid, ids, vals, context=context)
