# -*- coding: UTF-8 -*-

##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2016-Today Libre Comunication (<erpsystem.com.ua@gmail.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
import openerp
from openerp import SUPERUSER_ID
from openerp.osv import orm, osv, fields
from openerp.addons.web.http import request

import logging
_logger = logging.getLogger(__name__)

class website(orm.Model):
    _inherit = 'website'

    def sale_get_order(self, cr, uid, ids, force_create=False, code=None,
                       update_pricelist=None, context=None):
        sale_order_obj = super(website, self).sale_get_order(
            cr, uid, ids, force_create, code, update_pricelist, context=context)
        if sale_order_obj and sale_order_obj.state != 'draft':
            sale_order_obj = self.pool['sale.order']
            user = self.pool['res.users'].browse(
                cr, SUPERUSER_ID, uid, context=context)
            partner = user.partner_id
            team_pool = self.pool['crm.case.section']
            team_ids = team_pool.search(cr, uid, [])
            teams = team_pool.browse(
                cr, SUPERUSER_ID, team_ids, context=context)
            agent = False
            for team in teams:
                if user in team.member_ids:
                    agent = True
            if agent:
                user_id = uid
            else:
                user_id = 6
            values = {
                'user_id': user_id,
                'partner_id': partner.id,
                'pricelist_id': partner.property_product_pricelist.id,
                'section_id': self.pool.get(
                    'ir.model.data').get_object_reference(
                    cr, uid, 'website', 'salesteam_website_sales')[1],
            }
            sale_order_id = sale_order_obj.create \
                (cr, SUPERUSER_ID, values, context=context)
            values = sale_order_obj.onchange_partner_id(
                cr, SUPERUSER_ID, [], partner.id, context=context)['value']
            sale_order_obj.write(
                cr, SUPERUSER_ID, [sale_order_id], values, context=context)
            request.session['sale_order_id'] = sale_order_id
        return sale_order_obj

    def get_alternate_languages(self, cr, uid, ids, req=None, context=None):
        langs = []
        if req is None:
            req = request.httprequest
        default = self.get_current_website(
            cr, uid, context=context).default_lang_code
        shorts = []

        def get_url_localized(router, lang):
            try:
                arguments = dict(request.endpoint_arguments)
            except:
                arguments = dict({})

            for k, v in arguments.items():
                if isinstance(v, orm.browse_record):
                    arguments[k] = v.with_context(lang=lang)
            if arguments:
                return router.build(request.endpoint, arguments)
        router = request.httprequest.app.get_db_router(request.db).bind('')
        for code, name, image in self.get_languages(
                cr, uid, ids, context=context):
            lg_path = ('/' + code) if code != default else ''
            lg = code.split('_')
            shorts.append(lg[0])
            uri = request.endpoint \
                  and get_url_localized(router, code)\
                  or request.httprequest.path
            if req.query_string:
                uri += '?' + req.query_string

            url_root = req.url_root[0:-1]
            if 'https' not in url_root:
                url_root = url_root.replace('http', 'https')

            lang = {
                'hreflang': ('-'.join(lg)).lower(),
                'short': lg[0],
                'href': url_root + lg_path + uri,
            }
            langs.append(lang)
        for lang in langs:
            if shorts.count(lang['short']) == 1:
                lang['hreflang'] = lang['short']
        return langs

    @openerp.tools.ormcache(skiparg=3)
    def _get_languages(self, cr, uid, id):
        website = self.browse(cr, uid, id)
        return [(lg.code, lg.name, lg.image1) for lg in website.language_ids]

    def get_lang(self, cr, uid, ids, context=None):
        lang_pool = self.pool.get('res.lang')
        lang_id = lang_pool.search(
            cr, uid, [('code', '=', context.get('lang'))])
        language = lang_pool.browse(cr, uid, lang_id, context=context)
        return language


class IrHttp(orm.AbstractModel):
    _inherit = 'ir.http'

    def get_nearest_lang(self, lang):
        # Try to find a similar lang. Eg: fr_BE and fr_FR
        short = lang.partition('_')[0]
        short_match = False
        for code, name, image in request.website.get_languages():
            if code == lang:
                return lang
            if not short_match and code.startswith(short):
                short_match = code
        return short_match