# -*- coding: UTF-8 -*-

##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2016-Today Libre Comunication (<erpsystem.com.ua@gmail.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp import fields, models, api


class MassMailingList(models.Model):
    _inherit = "mail.mass_mailing.list"

    all = fields.Boolean(string="All",
                         default=True)
    user_only = fields.Boolean(string="Users Only")
    customer_only = fields.Boolean(string="Customers Only")
    supplier_only = fields.Boolean(string="Suppliers Only")

    @api.onchange('all')
    def onchange_all(self):
        if self.all:
            self.user_only = False
            self.customer_only = False
            self.supplier_only = False

    @api.onchange('user_only')
    def onchange_user_only(self):
        if self.user_only:
            self.all = False

    @api.onchange('customer_only')
    def onchange_customer_only(self):
        if self.customer_only:
            self.all = False

    @api.onchange('supplier_only')
    def onchange_supplier_only(self):
        if self.customer_only:
            self.all = False

    @api.model
    def create(self, values):
        user_list = []
        customer_list = []
        supplier_list = []
        mass_mailing = super(MassMailingList, self).create(values)
        if values.get('user_only'):
            user_list = self.get_user()
        if values.get('customer_only'):
            customer_list = self.get_partner('customer')
        if values.get('supplier_only'):
            supplier_list = self.get_partner('supplier')
        if values.get('all'):
            user_list = self.get_user()
            customer_list = self.get_partner('customer')
            supplier_list = self.get_partner('supplier')
        mail_list = user_list + customer_list + supplier_list
        mass_mailing_contact_pool = self.env['mail.mass_mailing.contact']
        for person in mail_list:
            if person.get('name') and person.get('email'):
                vals = {
                    'name': person.get('name'),
                    'email': person.get('email'),
                    'list_id': mass_mailing.id,
                }
                mass_mailing_contact_pool.create(vals)
        return mass_mailing

    def get_user(self):
        res_users = self.env['res.users'].search([('active', '=', True)])
        return [{'name': user.name, 'email': user.email} for user in res_users]

    def get_partner(self, partner):
        res_partners = self.env['res.partner'].search([
            ('active', '=', True), (partner, '=', True)])
        return [{'name': res_partner.name, 'email': res_partner.email}
                for res_partner in res_partners]
