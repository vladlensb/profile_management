# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Business Applications
#    Copyright (c) 2012 OpenERP S.A. <http://openerp.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from . import res_users
from . import product_material
from . import product_optimal_qty
from . import product_template
from . import sale
from . import competitors
from . import product_attribute_line
from . import foam_dimensions
from . import mass_mailing_stats
from . import res_partner
from . import res_language
from . import auth_oauth
from . import pricelist
from . import website
from . import crm_lead
from . import mass_mailing
from . import further_processing
from . import stock
from . import purchase
from . import production_line_type
# import mail_thread
from . import stock_transfer_details
