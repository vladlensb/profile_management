# -*- coding: UTF-8 -*-

##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2016-Today Libre Comunication (<erpsystem.com.ua@gmail.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

{
    'name': 'Profile Management',
    'version': '1.0',
    'author': 'THERMEVO',
    'website': 'https://THERMEVO.de/',
    'category': 'Profile Management',
    'sequence': 8,
    'depends': [
        'base',
        'account',
        'product',
        'mass_mailing',
        'web',
        'mrp',
        'hr',
        'website',
        'crm',
        'sale',
        'purchase',
        'auth_oauth',
        'auth_openid',
        'auth_signup',
        'website_sale',
        'website_crm',
        'web_tree_image',
        'website_quote',
        'sgeede_b2b',

    ],
    'description': "",
    'data': ['security/profile_management_security.xml',
             'security/ir.model.access.csv',
             'views/report_saleorder_inherit.xml',
             'views/sale_report_inherit.xml',
             'views/product_template.xml',
             'views/product_material_views.xml',
             'views/sale_view.xml',
             'views/competitors_product_view.xml',
             'views/res_partner_view.xml',
             'views/mass_mailing.xml',
             'views/res_language_view.xml',
             'views/auth_oauth_view.xml',
             'views/res_users_view.xml',
             'views/pricelist_view.xml',
             'views/res_country_view.xml',
             'views/crm_lead_views.xml',
             'views/stock_picking_views.xml',
             'views/further_processing_views.xml',
             'views/purchase_views.xml',
             'views/production_scheme_config_view.xml',

             'views/report_invoice.xml',
             ],
    'installable': True,
    'auto_install': False,
    'application': True,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
